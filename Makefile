BUILDDIR  := build
SOURCEDIR := src
CFLAGS    := -ggdb -std=gnu99
LDFLAGS   := -Wall -Werror -lnet -lpcap -lssl -lcrypto -lcunit -lpthread -lrt -liniparser
EXEC 	  := $(BUILDDIR)/mptcptest
# SOURCES := relative paths of *.c files in $(SOURCDIR)
SOURCES   := $(shell cd $(SOURCEDIR); find -name '*.c' ! -name 'main_server.c' | sed 's|^\./||')
OBJECTS   := $(addprefix $(BUILDDIR)/,$(SOURCES:%.c=%.o))
DEPEND    := $(addprefix $(BUILDDIR)/,$(SOURCES:%.c=%.d))
CC        := gcc

all: $(EXEC)

$(EXEC): $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS)

$(BUILDDIR)/%.d: $(SOURCEDIR)/%.c
	@mkdir -p $(dir $@) 
	gcc -MM $(CFLAGS) $<|sed -e '1s|.|$(dir $@)&|' \
	|sed '$$a\\t$(CC) -o $(@:.d=.o) $(CFLAGS) $(LDFLAGS) -c $<' > $@

-include $(DEPEND)

depend: $(DEPEND)

server: src/main_server.c src/server.h
	$(CC) -o server src/main_server.c -ggdb
	
test: src/trace.c src/trace.h
	$(CC) -o trace src/trace.c -ggdb

.PHONY: test clean mrproper

clean:
	find $(BUILDDIR) -name "*.[do]" -exec rm {} \;
	find $(BUILDDIR) -type d -depth -empty -exec rmdir "{}" \;
	rm $(EXEC)

mrproper:
	rm $(EXEC)

run: $(EXEC)
	sudo ./$(EXEC)