#!/bin/bash
# prevents the kernel from aborting connections it does not hold a state for
sudo iptables -A INPUT -s 10.2.1.2 -p tcp --sport 6825 -j DROP
sudo iptables -A INPUT -s 10.2.1.2 -p tcp --dport 6826 -j DROP
sudo iptables -A INPUT -s 10.2.1.3 -p tcp --sport 6825 -j DROP
sudo iptables -A INPUT -s 10.2.1.3 -p tcp --dport 6826 -j DROP
sudo ifconfig tap2:0 10.2.1.11
sudo build/mptcptest