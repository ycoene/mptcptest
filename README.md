Instructions
============
Following are the instructions for setting up the test environment and running the test suite.

Linux MPTCP UML
---------------

 1. Retreive or compile a MPTCP-enabled UML image by following the steps explained [here](http://multipath-tcp.org/pmwiki.php/Users/UML).

 2. Launch the UML image

        $ cd $UML_DIR
        $ ./server.sh

 3. Copy `mptcptest` to the UML

        $ scp -r mptcptest root@10.2.1.2:/root

 4. On the UML host, compile and run the slave

        # apt-get update
        # apt-get install build-essential
        # cd /root/mptcptest
        # make server
        # ./server

 5. Build the master (depends on libnet, libpcap, cunit, openssl and iniparser)

        $ make

 6. Run the master (requires root privileges)

        $ ./mptcptest.sh

Generic
-------

To adapt the instructions above to a generic environment, make sure to modify

 * `config/mptcptest.ini` to match the local and remote addresses;
 * `mptcptest.sh` to update IPTables rules;
 * `src/main_server.c` at line 159 to match the host #0 address.