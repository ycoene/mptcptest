/**
 * @file      alternative.h
 * @brief     Facilities for exploring an alternative tree.
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 *
 * Useful for crafting packets with various combinations of properties.
 *
 * Example:
 *
 * \code
 * char l1[] = {'a', 'b'};
 * char l2[] = {'c', 'd'};
 *
 * ALT_GROUP(g)
 * {
 *   ALT_GROUP_PREAMBLE(g);
 *
 *   char l1_curr = ALT(&g, 1, l1);
 *   printf("%c", l1_curr);
 *
 *   if(l1_curr == 'a')
 *     printf("%c", ALT(&g, 2, l2););
 * }
 * \endcode
 *
 * This code stub outputs sequences "ac", "ad" and "b".
 */

#ifndef ALTERNATIVE_H_
#define ALTERNATIVE_H_

#define ALT_N_MAX 16

struct alt_grp {
  int last;
  int take_any_path;
  struct {
    int i; // indice
    int exhausted;
  } n[ALT_N_MAX]; // n
};

#define ALT_GROUP_INIT(grp_name) struct alt_grp grp_name = {}

#define ALT_GROUP_EVAL(grp) while(grp.n[0].exhausted == 0)

#define ALT_GROUP(grp_name) ALT_GROUP_INIT(grp_name); ALT_GROUP_EVAL(grp_name)

#define ALT_GROUP_PREAMBLE(grp) {  \
  grp.take_any_path = 0;                \
  grp.last = 0;                         \
  grp.n[0].exhausted = 1;               \
}

int alt(struct alt_grp *grp, int alt_id, int card);

/**
 * \param alt_id a unique (wrt. \c grp) id ranging from 1 to ALT_N_MAX
 * \param grp input group
 * \param array
 * \return a member of \c array
 */
#define ALT(grp, alt_id, array) (array[alt(grp, alt_id, sizeof(array)/sizeof(array[0]))])

#endif /* ALTERNATIVE_H_ */
