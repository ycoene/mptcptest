/**
 * @file      mptcp.h
 * @brief     MPTCP constants, data structures and conversion functions
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#ifndef MPTCP_H_
#define MPTCP_H_

#include <libnet.h>
#include <pcap.h>
#include <sys/queue.h>
#include "barrier.h"

#define MAX_NB_SF   16

/* TCP options */

#define TCP_OPT_MPTCP   0x1E
#define TCP_OPT_NOP     0x01
#define TCP_OPT_WS      0x03

/* (MP)TCP FSM states */

enum conn_state {
  CLOSED,
  LISTEN,
  SYN_SENT,
  SYN_RECEIVED,
  ESTABLISHED,
  CLOSE_WAIT,
  LAST_ACK,
  FIN_WAIT_1,
  FIN_WAIT_2,
  CLOSING,
  TIME_WAIT
};

/* Multipath Capable subtype flags */

#define MP_CAP_A 0x80
#define MP_CAP_B 0x40
#define MP_CAP_C 0x20
#define MP_CAP_D 0x10
#define MP_CAP_E 0x08
#define MP_CAP_F 0x04
#define MP_CAP_G 0x02
#define MP_CAP_H 0x01

/* Multipath Join subtype flags */

#define MP_JOIN_SYN_RSVD_MASK   0x0E
#define MP_JOIN_ACK_RSVD_1      0x0F
#define MP_JOIN_SYN_B           0x01

/* DSS subtype flags */

#define DSS_F   0x10
#define DSS_m   0x08
#define DSS_M   0x04
#define DSS_a   0x02
#define DSS_A   0x01

/*  MPTCP option subtype values */

#define MPTCP_SUBTYPE_MP_CAPABLE    0x0 /**< Multipath Capable */
#define MPTCP_SUBTYPE_MP_JOIN       0x1 /**< Join Connection */
#define MPTCP_SUBTYPE_DSS           0x2 /**< Data Sequence Signal ACK and Data Sequence Mapping) */
#define MPTCP_SUBTYPE_ADD_ADDR      0x3 /**< Add Address */
#define MPTCP_SUBTYPE_REMOVE_ADDR   0x4 /**< Remove Address */
#define MPTCP_SUBTYPE_MP_PRIO       0x5 /**< Change Subflow Priority */
#define MPTCP_SUBTYPE_MP_FAIL       0x6 /**< Fallback */
#define MPTCP_SUBTYPE_MP_FASTCLOSE  0x7 /**< Fast close */

#define MPTCP_VERSION 0x0

/**
 * A key is represented as a byte array of length 8.
 */
typedef struct { uint8_t data[8]; } key64;
typedef struct { uint8_t data[4]; } token32;

void *get_mptcp_subt(int subtype);

typedef struct {
  uint8_t type;
  uint8_t length;
  uint8_t subtype; /**< subtype (0-3) */
} mptcp_hdr_pre;

/* DSS Option */

typedef struct {
  uint8_t type;
  uint8_t length;
  uint8_t sub_res; /**< subtype (0-3) and reserved (4-7) */
  uint8_t res_flags; /**< reserved (0-2) and flags (3-7) */
} dss_net_header;

typedef struct {
  uint32_t ssn; /**< subflow sequence number */
  uint16_t dll; /**< Data-Level Length */
  uint8_t checksum[2]; /** Checksum */
} dss_net_footer;

typedef struct {
  dss_net_header header;
  uint8_t footer[24];
} dss_net;

typedef struct {
  uint subtype;
  uint flags;
  uint dack_len;
  uint dsn_len;
  uint64_t dack;
  uint64_t dsn;
  uint ssn;
  uint dll;
  uint8_t checksum[2];
} dss_loc;

typedef struct {
  uint64_t dsn; /**< Data Sequence Number */
  uint32_t ssn; /**< Subflow Sequence Number */
  uint16_t data_len; /**< Data-Level Length */
  uint16_t zeros;
} dss_pseudo_header_net;

dss_net dss_enc(dss_loc *dssl);
dss_loc dss_dec(dss_net *dssn);

typedef struct subflow {
  u_int32_t laddr; /**< local ip address */
  u_int32_t raddr; /**< remote ip address */
  u_int16_t lport; /**< local port number */
  u_int16_t rport; /**< remote port number */
  enum conn_state state; /**< current state of the TCP FSM */
  uint mss; /**< maximum segment size */
  uint snd_nxt;
  uint snd_una;
  uint snd_wnd;
  uint rcv_nxt;
  uint rcv_wnd;
  uint isn;

  uint id;
  int dupack_count;

  struct {
    int syn_without_mp_cap;
    int synack_without_mp_cap;
    u_char mp_cap_version;
    u_char mp_cap_flags_ts;
    u_char mp_cap_flags_sut;
  } ci_o; //< options for connection initiation

  struct {
    token32 lnonce;
    token32 rnonce;
    int syn_unknown_token;
    int syn_b;
    int ack_without_mp_join;
    int ack_unexpected_hmac;
  } ns_o; //< options for new subflows

  int outstanding_sut_ts_data;

  libnet_t *l; /**< libnet context */
  libnet_ptag_t ptag_ip;
  libnet_ptag_t ptag_tcp;
  libnet_ptag_t ptag_tcp_opt;
  pcap_t *p; /**< pcap session handle */
  pthread_t t;

} subflow;

dss_loc dss_autofill(int dack_len, int dsn_len, uint8_t *dsn_map_data, int dll, uint32_t ssn, int dfin_set);
void dss_do_checksum(dss_loc *dssl, uint8_t *dsn_map_data);

/**
 * Initialize a new subflow.
 * @return new subflow
 */
subflow *subflow_init(char *devname);

void subflow_free(subflow *sf);

typedef struct flow {
  enum conn_state state; /**< connection state */
  key64 lkey; /**< local key */
  key64 rkey; /**< remote key */
  token32 ltok; /**< local token */
  token32 rtok; /**< remote token */
  u_int64_t snd_nxt; /**< data sequence number */
  u_int64_t snd_una;
  uint rcv_wnd;
  u_int64_t rcv_nxt; /**< data ACK number */
  int nsf; /**< number of subflows */
  subflow *sf[MAX_NB_SF];
  subflow *asf; /**< active subflow */
  uint8_t inc_csum[2]; /**< incremental checksum */
  int no_checksum;

  int hole_in_sent_data;
  int ct_combined_fin_data;
  int ct_combined_fin_ack;
  subflow *ct_combined_data_sf_fin_sut_ts;
  subflow *ct_combined_data_sf_fin_ts_sut;

  /** handler list for expected packets */
  LIST_HEAD(hlist, handler) hl_head;
} flow;

/**
 * Initialize a new flow.
 * @return new flow
 */
flow *flow_init();

char errbuf[LIBNET_ERRBUF_SIZE+PCAP_ERRBUF_SIZE];

/**
 * Network representation of the MP_CAPABLE option. Used only as an input/output
 * of functions tcp_opt_mp_capable_dec() and tcp_opt_mp_capable_enc().
 */
typedef struct mp_cap_net {
  uint8_t type;
  uint8_t length;
  uint8_t subver; /**< subtype (0-3) and version (4-7) */
  uint8_t flags; /**< A|B|C|D|E|F|G|H */
  key64 skey; /**< sender's key */
  key64 rkey; /**< receiver's key */
} mp_cap_net;

/**
 * Local representation of the MP_CAPABLE option. Not to send on the wire as is.
 */
typedef struct mp_cap_loc {
  uint subtype;
  uint version;
  uint flags;
  key64 skey; /**< sender's key */
  key64 rkey; /**< receiver's key */
} mp_cap_loc;

mp_cap_net mp_cap_enc(mp_cap_loc const *o, uint tcp_flags);
mp_cap_loc mp_cap_dec(mp_cap_net const *o);

typedef struct mp_join_net {
  uint8_t type;
  uint8_t length;
  uint8_t subb; /**< subtype (0-3) and B (7) */
  uint8_t addrid; /**< Address ID */
  union {
    struct { /**< initial SYN */
      token32 rtok; /**< Receiver's Token */
      token32 snonce_syn; /**< Sender's Random Number */
    };
    struct { /**< responding SYN/ACK */
      key64 sthmac; /**< Sender's truncated HMAC */
      token32 snonce_synack; /**< Sender's Random Number */
    };
    struct { /**< third ACK */
      uint8_t shmac[20];
    };
  };
} mp_join_net;

typedef struct mp_join_loc {
  uint b; /**< B */
  uint addrid; /**< Address ID */
  token32 rtok; /**< Receiver's Token */
  token32 snonce; /**< Sender's Random Number */
  key64 sthmac;
  uint8_t shmac[20];
} mp_join_loc;

mp_join_net mp_join_enc(mp_join_loc const *o, uint tcp_flags);
mp_join_loc mp_join_dec(mp_join_net const *o, uint tcp_flags);

typedef struct mp_fastclose_net {
  uint8_t kind;
  uint8_t length;
  uint8_t subres;
  uint8_t res;
  key64 rkey;
} mp_fastclose_net;

mp_fastclose_net mp_fastclose_enc(key64 key);

/**
 * ADD_ADDR option for IPv4
 */
typedef struct add_addr_net {
  uint8_t kind;
  uint8_t length;
  uint8_t subipver;
  uint8_t addrid;
  uint32_t addr;
  // uint16_t port;
} add_addr_net;

/**
 * Expand \c lsb to the 64-bit value nearest from \c ref whose least
 * significant 32 bits are \c lsb.
 */
uint64_t dsn_expand(uint64_t ref, uint32_t lsb);

#endif /* MPTCP_H_ */
