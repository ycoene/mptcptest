/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "test_ct_noreorder.h"

static int enabled(token_t *this)
{
  return !s.dfin_sent;
}

static void on_set(token_t *this)
{
  s.dfin_sent = 1;
  s.sent_dfin_seq_inc.dack = f->snd_nxt + 1;
  if(!s.dss_no_repeat) on_set_snd_dack();
}

static void action(token_t *this)
{
  sf_spec_evt *e = (sf_spec_evt *) this;
  dss_loc dssl = dss_autofill(4, 4, NULL, 1, 0, 1);
  b_send_data(f->sf[e->sfid], &dssl, NULL, 0, TH_ACK);
}

token_t *new_evt_snd_dfin(int sfid)
{
  sf_spec_evt *evt = malloc(sizeof(sf_spec_evt));
  sprintf(evt->to_str, "sDF%i", sfid);
  *(token_t *) evt = (token_t) {
    .type = ACTIVE,
    .to_str = evt->to_str,
    .enabled = enabled,
    .on_set = on_set,
    .f = action,
  };
  evt->sfid = sfid;
  return (token_t *) evt;
}
