/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "test_ct_noreorder.h"

static int enabled(token_t *this)
{
  sf_spec_evt *e = (sf_spec_evt *) this;
  return s.dfin_sent && s.dack_of_dfin_sent && !s.fin_sent[e->sfid];
}

static void on_set(token_t *this)
{
  sf_spec_evt *e = (sf_spec_evt *) this;
  s.fin_sent[e->sfid] = 1;
  s.sent_fin_seq_inc[e->sfid].ack = f->sf[e->sfid]->snd_nxt+1;
  if(!s.dss_no_repeat) on_set_snd_dack();
}

static void action(token_t *this)
{
  sf_spec_evt *e = (sf_spec_evt *) this;
  dss_loc *dsslp;
  if(!s.dss_no_repeat)
  {
    dss_loc dssl = dss_autofill(4, 0, NULL, 0, 0, 0);
    dsslp = &dssl;
  }
  else dsslp = NULL;
  b_send_data(f->sf[e->sfid], dsslp, NULL, 0, TH_FIN|TH_ACK);
}

token_t *new_evt_snd_fin(int sfid)
{
  sf_spec_evt *evt = malloc(sizeof(sf_spec_evt));
  sprintf(evt->to_str, "sF%i", sfid);
  *(token_t *) evt = (token_t) {
    .type = ACTIVE,
    .to_str = evt->to_str,
    .enabled = enabled,
    .on_set = on_set,
    .f = action,
  };
  evt->sfid = sfid;
  return (token_t *) evt;
}
