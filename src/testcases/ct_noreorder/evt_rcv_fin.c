/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "test_ct_noreorder.h"

static int enabled(token_t *this)
{
  /* Essentially, a host MUST NOT close all functioning subflows unless
   * it is safe to do so, i.e., until all outstanding data has been
   * DATA_ACKed, or until the segment with the DATA_FIN flag set is the
   * only outstanding segment. */

  err_str = "Subflows termination";

  if(s.established_sut_ts_sf_count-1 == 0) // all subflows closed
    return s.dfin_recvd &&
        (!s.data_recvd || s.recvd_data_dacked || s.recvd_dfin_data_combined);
  else
    return 1;
}

static void on_set(token_t *this)
{
  sf_spec_evt *e = (sf_spec_evt *) this;
  if(!s.fin_recvd[e->sfid])
    s.established_sut_ts_sf_count--;
  s.fin_recvd[e->sfid] = 1;
}

static int must_arrive(token_t *this)
{
  /* Once a DATA_FIN has been acknowledged, all remaining subflows
   * MUST be closed with standard FIN exchanges. */

  sf_spec_evt *e = (sf_spec_evt *) this;
  return s.dack_of_dfin_sent && !s.fin_recvd[e->sfid];
}

token_t *new_evt_rcv_fin(int sfid)
{
  sf_spec_evt *evt = malloc(sizeof(sf_spec_evt));
  sprintf(evt->to_str, "rF%i", sfid);
  *(token_t *) evt = (token_t) {
    .type = PASSIVE,
    .to_str = evt->to_str,
    .enabled = enabled,
    .on_set = on_set,
    .must_arrive = must_arrive,
    .h = {.f = c_recv_fin, .sf = f->sf[sfid]}
  };
  evt->sfid = sfid;
  return (token_t *) evt;
}
