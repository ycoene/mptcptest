/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "test_ct_noreorder.h"

static int enabled(token_t *this)
{
  sf_spec_evt *e = (sf_spec_evt *) this;
  err_str = "ACK[FIN] received but no FIN sent";
  return s.fin_sent[e->sfid];
}

static void on_set(token_t *this)
{
  sf_spec_evt *e = (sf_spec_evt *) this;
  s.final_ack_recvd[e->sfid] = 1;
}

static int must_arrive(token_t *this)
{
  sf_spec_evt *e = (sf_spec_evt *) this;
  return s.fin_sent[e->sfid] && !s.final_ack_recvd[e->sfid];
}

token_t *new_evt_rcv_ack(int sfid)
{
  sf_spec_evt *evt = malloc(sizeof(sf_spec_evt));
  sprintf(evt->to_str, "rA[F]%i", sfid);
  *(token_t *) evt = (token_t) {
    .type = PASSIVE,
    .to_str = evt->to_str,
    .enabled = enabled,
    .on_set = on_set,
    .must_arrive = must_arrive,
    .h = {.f = c_recv_ack, .sf = f->sf[sfid], .args = &s.sent_fin_seq_inc[sfid]},
  };
  evt->sfid = sfid;
  return (token_t *) evt;
}
