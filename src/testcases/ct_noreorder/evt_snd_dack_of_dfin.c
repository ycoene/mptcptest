/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "test_ct_noreorder.h"

static int enabled(token_t *this)
{
  return s.dfin_recvd && !s.dack_of_dfin_sent;
}

static void on_set(token_t *this)
{
  s.dack_of_dfin_sent = 1;
  on_set_snd_dack();
}

static void action(token_t *this)
{
  sf_spec_evt *e = (sf_spec_evt *) this;
  dss_loc dssl = dss_autofill(4, 0, NULL, 0, 0, 0);
  b_send_data(f->sf[e->sfid], &dssl, NULL, 0, TH_ACK);
}

token_t *new_evt_snd_dack_of_dfin(int sfid)
{
  sf_spec_evt *evt = malloc(sizeof(sf_spec_evt));
  sprintf(evt->to_str, "sDA[DF]%i", sfid);
  *(token_t *) evt = (token_t) {
    .type = ACTIVE,
    .to_str = evt->to_str,
    .enabled = enabled,
    .on_set = on_set,
    .f = action,
  };
  evt->sfid = sfid;
  return (token_t *) evt;
}
