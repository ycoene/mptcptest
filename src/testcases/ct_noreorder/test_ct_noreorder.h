/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 * @file      test_ct_noreorder.h
 * @brief     test connection termination
 */

#ifndef TEST_CT_NOREORDER_
#define TEST_CT_NOREORDER_

#include "../../testcases.h"
#include "../../trace_explore.h"

/**
 * \c s characterizes the state of the connection during the termination
 * procedure. State variables are named from the test system viewpoint.
 * Following that convention, an event can be
 *
 *  - sent (TS->SUT)
 *
 *    keywords: \c _sent, \c snd_
 *
 *  - received (SUT->TS)
 *
 *    keywords: \c _recvd, \c rcv_
 *
 * Furthermore, a segment can be \e built but not sent directly in order to
 * simulate packet reordering. It may conceptually be seen as being "on its
 * way" to the SUT.
 */

struct state {
  mptcp_seg dfin;

  /* DATA_FIN */
  int dfin_sent;
  int dfin_recvd;

  /* DATA_ACK[DATA_FIN] */
  int dack_of_dfin_sent;
  int dack_of_dfin_recvd;
  struct c_recv_dack_arg sent_dfin_seq_inc;

  int established_sut_ts_sf_count;

  /* Subflow FIN */
  int fin_sent[2];
  int fin_recvd[2];

  /* Subflow ACK[FIN] */
  int final_ack_sent[2];
  int final_ack_recvd[2];
  struct c_recv_ack_arg sent_fin_seq_inc[2];

  /* Received data */
  int cmd_send_data_sent;
  int data_recvd;
  int data_recvd_sf[2];
  int recvd_dfin_data_combined;
  CF_ARG_TYPE(c_recv_dsn) expected_data_dsn;
  int recvd_data_dacked;

  /* Sent data */
  int data_sent;
  int sent_data_acked;
  CF_ARG_TYPE(c_recv_dack) sent_data_dsn_inc;

  /* Remote close */
  int cmd_close_sent;

  /* test options */
  int dss_no_repeat; // don't repeat DSS ACK on FIN and ACK
  int reorder;
} s;

void on_set_snd_dack();

typedef struct sf_spec_evt {
  token_t e;
  int sfid;
  char to_str[16];
} sf_spec_evt;

token_t *new_evt_rcv_ack(int sfid);
token_t *new_evt_rcv_dack_of_data();
token_t *new_evt_rcv_dack_of_dfin();
token_t *new_evt_rcv_data();
token_t *new_evt_rcv_dfin();
token_t *new_evt_rcv_fin(int sfid);
token_t *new_evt_remote_snd_data();
token_t *new_evt_snd_dack_of_data();
token_t *new_evt_snd_dack_of_dfin(int sfid);
token_t *new_evt_snd_data();
token_t *new_evt_snd_dfin(int sfid);
token_t *new_evt_snd_fin(int sfid);
token_t *new_evt_snd_fin(int sfid);
token_t *new_evt_snd_ack(int sfid);
token_t *new_evt_remote_close();
token_t *new_evt_snd_dack_of_data();

#endif /* TEST_CT_NOREORDER_H_ */
