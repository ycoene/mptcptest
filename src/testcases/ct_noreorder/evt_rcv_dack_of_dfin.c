/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "test_ct_noreorder.h"

static int enabled(token_t *this)
{
  err_str = "DACK[DFIN] received but no DFIN sent";
  return s.dfin_sent;
}

static void on_set(token_t *this)
{
  s.dack_of_dfin_recvd = 1;
}

static int must_arrive(token_t *this)
{
  return s.dfin_sent && !s.dack_of_dfin_recvd;
}

token_t *new_evt_rcv_dack_of_dfin()
{
  token_t *evt = malloc(sizeof(token_t));
  *evt = (token_t) {
    .type = PASSIVE,
    .to_str = "rDA[DF]",
    .enabled = enabled,
    .on_set = on_set,
    .must_arrive = must_arrive,
    .h = {.f = c_recv_dack, .args = &s.sent_dfin_seq_inc}
  };
  return evt;
};
