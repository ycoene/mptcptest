/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "test_ct_noreorder.h"

static int enabled(token_t *this)
{
  sf_spec_evt *e = (sf_spec_evt *) this;
  return s.fin_recvd[e->sfid];
}

static void on_set(token_t *this)
{
  sf_spec_evt *e = (sf_spec_evt *) this;
  s.final_ack_sent[e->sfid] = 1;
  if(!s.dss_no_repeat) on_set_snd_dack();
}

static void action(token_t *this)
{
  sf_spec_evt *e = (sf_spec_evt *) this;
  dss_loc *dsslp;
  if(!s.dss_no_repeat)
  {
    dss_loc dssl = dss_autofill(4, 0, NULL, 0, 0, 0);
    dsslp = &dssl;
  }
  else dsslp = NULL;
  b_send_data(f->sf[e->sfid], dsslp, NULL, 0, TH_ACK);
}

token_t *new_evt_snd_ack(int sfid)
{
  sf_spec_evt *evt = malloc(sizeof(sf_spec_evt));
  sprintf(evt->to_str, "sA[F]%i", sfid);
  *((token_t *) evt) = (token_t) {
    .type = ACTIVE,
    .to_str = evt->to_str,
    .enabled = enabled,
    .on_set = on_set,
    .f = action,
  };
  evt->sfid = sfid;
  return (token_t *) evt;
}
