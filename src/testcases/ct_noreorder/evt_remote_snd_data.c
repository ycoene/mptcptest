/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "test_ct_noreorder.h"

static int enabled(token_t *this)
{
  return !s.cmd_close_sent;
}

static void on_set(token_t *this)
{
  s.cmd_send_data_sent = 1;
  s.expected_data_dsn.dsn = f->rcv_nxt;
  s.expected_data_dsn.dll = 1;
}

static void action(token_t *this)
{
  b_remote_send_data(1, 0);
}

token_t *new_evt_remote_snd_data()
{
  token_t *evt = malloc(sizeof(token_t));
  *evt = (token_t) {
      .type = ACTIVE,
      .to_str = "rmD",
      .enabled = enabled,
      .on_set = on_set,
      .f = action
  };
  return evt;
}
