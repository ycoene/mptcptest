/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "test_ct_noreorder.h"

int enabled(token_t *this)
{
  return s.data_recvd && !s.recvd_data_dacked;
}

void on_set(token_t *this)
{
  s.recvd_data_dacked = 1;
  on_set_snd_dack();
}

void action(token_t *this)
{
  dss_loc dssl = dss_autofill(4, 0, NULL, 0, 0, 0);
  b_send_data(f->sf[0], &dssl, NULL, 0, TH_ACK);
}

token_t *new_evt_snd_dack_of_data()
{
  token_t *evt = malloc(sizeof(token_t));
  *evt = (token_t) {
    .type = ACTIVE,
    .to_str = "sDA[D]()",
    .enabled = enabled,
    .on_set = on_set,
    .f = action
  };
  return evt;
};
