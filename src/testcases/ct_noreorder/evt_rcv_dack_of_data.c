/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "test_ct_noreorder.h"

static int enabled()
{
  return s.data_sent;
}

static int must_arrive()
{
  return s.data_sent && !s.sent_data_acked;
}

static void on_set()
{
  s.sent_data_acked = 1;
}

token_t *new_evt_rcv_dack_of_data()
{
  token_t *evt = malloc(sizeof(token_t));
  *evt = (token_t) {
    .type = PASSIVE,
    .to_str = "rDA[D]",
    .enabled = enabled,
    .on_set = on_set,
    .must_arrive = must_arrive,
    .h = {.f = c_recv_dack, .args = &s.sent_data_dsn_inc}
  };
  return evt;
};
