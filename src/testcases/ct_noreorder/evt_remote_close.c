/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "test_ct_noreorder.h"

static int enabled()
{
  return !s.cmd_close_sent;
}

static void on_set()
{
  s.cmd_close_sent = 1;
}

static void action()
{
 // When data sent before issuing a remote close command, the SUT appears to
 // close the connection before having read the data, which triggers a
 // FAST_CLOSE. Calling sleep() gives the SUT some time to read the data first.
  usleep(15e3);
  b_remote_send_data(0, 1);
}

token_t *new_evt_remote_close()
{
  token_t *evt = malloc(sizeof(token_t));
  *evt = (token_t) {
    .type = ACTIVE,
    .to_str = "rmc()",
    .enabled = enabled,
    .on_set = on_set,
    .f = action
  };
  return evt;
};
