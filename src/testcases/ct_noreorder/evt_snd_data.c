/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "test_ct_noreorder.h"

static int enabled(token_t *this)
{
  return !s.dfin_sent && !s.cmd_close_sent;
}

static void on_set(token_t *this)
{
  s.sent_data_dsn_inc.dack = f->snd_nxt + 1;
  s.data_sent = 1;
}

static void action(token_t *this)
{
  u_char data[] = {'D'};
  dss_loc dssl = dss_autofill(4, 4, data, sizeof(data), 1, 0);
  b_send_data(f->sf[0], &dssl, data, sizeof(data), TH_ACK);
}

token_t *new_evt_snd_data()
{
  token_t *evt = malloc(sizeof(token_t));
  *evt = (token_t) {
    .type = ACTIVE,
    .to_str = "sD",
    .enabled = enabled,
    .on_set = on_set,
    .f = action
  };
  return evt;
};
