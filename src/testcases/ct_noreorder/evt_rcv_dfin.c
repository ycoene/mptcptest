/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "test_ct_noreorder.h"

static int enabled(token_t *this)
{
  /* It is only permissible to combine these signals on one subflow if
   * there is no data outstanding on other subflows. */

  int combined_with_fin = (get_tcp_hdr(this->h.packet)->flags & TH_FIN);
  int outstanding_sutts_data_other_sf = 0;

  for(int i = 0; i < 2; i++)
    if(this->h.in_sf->id != i)
      if(s.data_recvd_sf[i] && !s.recvd_data_dacked)
        outstanding_sutts_data_other_sf = 1;

  if(!s.cmd_close_sent)
  {
    err_str = "DFIN received but closed() has not been called";
    return 0;
  }
  else if(combined_with_fin && outstanding_sutts_data_other_sf)
  {
    err_str = "Combined DATA_FIN and subflow FIN";
    return 0;
  }

  return 1;
}

static void on_set(token_t *this)
{
  s.dfin_recvd = 1;
}

static int must_arrive(token_t *this)
{
  /* A DATA_FIN is an indication that the sender has no more data to send. */

  return s.cmd_close_sent && !s.dfin_recvd;
}

token_t *new_evt_rcv_dfin()
{
  token_t *evt = malloc(sizeof(token_t));
  *evt = (token_t) {
    .type = PASSIVE,
    .to_str = "rDF",
    .enabled = enabled,
    .on_set = on_set,
    .must_arrive = must_arrive,
    .h = {.f = c_recv_data_fin, .copy_packet = 1}
  };
  return evt;
};
