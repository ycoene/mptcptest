/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "test_ct_noreorder.h"
#include <stddef.h>

void on_set_snd_dack()
{
  static int first_time = 1;
  static token_t *snd_dack_of_dfin;
  static token_t *snd_dack_of_data;

  if(first_time)
  {
    snd_dack_of_dfin = new_evt_snd_dack_of_dfin(0);
    snd_dack_of_data = new_evt_snd_dack_of_data(0);
    first_time = 0;
  }

  if(snd_dack_of_dfin->enabled(snd_dack_of_dfin))
    snd_dack_of_dfin->on_set(snd_dack_of_dfin);
  if(snd_dack_of_data->enabled(snd_dack_of_data))
    snd_dack_of_data->on_set(snd_dack_of_data);
}

/* end of trace condition for a single-subflow connection */
int end_trace_cond_1sf()
{
  return s.final_ack_sent[0] && s.final_ack_recvd[0];
}

/* end of trace condition for a connection with 2 subflows */
int end_trace_cond_2sf()
{
  return s.final_ack_sent[0] && s.final_ack_recvd[0] &&
      s.final_ack_sent[1] && s.final_ack_recvd[1];
}

/* Explore paths for a single-sublow connection termination */
void ts_close_explore_1sf()
{
  test_case_init(1);

//  int dss_on_fin;
  token_t *token_ct_1sf[] = {
      new_evt_rcv_dfin(),
      new_evt_rcv_dack_of_dfin(),
      new_evt_rcv_fin(0),
      new_evt_rcv_ack(0),
      new_evt_rcv_data(),
      new_evt_rcv_dack_of_data(),
      new_evt_snd_dfin(0),
      new_evt_snd_dack_of_dfin(0),
      new_evt_snd_fin(0),
      new_evt_snd_ack(0),
      new_evt_remote_close(),
      new_evt_remote_snd_data(),
      new_evt_snd_dack_of_data(),
      new_evt_snd_data()
  };

  struct trace a_tr_trace;
  TAILQ_INIT(&a_tr_trace);
  int ret/*, i=0*/;
  printf("\n");

  while(ret != TEST_DONE /*&& i++<250*/)
  {
    init_sf_handshake(f->sf[0]);

    // initialize state
    s = (struct state){.established_sut_ts_sf_count=1};

    ret = explore_trace(
        token_ct_1sf, sizeof(token_ct_1sf),
        end_trace_cond_1sf,
        &a_tr_trace, 0/*1*/);

    if(ret == TEST_STOP || ret == TEST_DONE)
    {
      b_flow_fastclose();
    }
  }

  test_case_free();
}

void ts_close_explore_2sf()
{
  test_case_init(2);

//  int dss_on_fin;
  token_t *token_ct_2sf[] = {
      new_evt_rcv_dfin(),
      new_evt_rcv_dack_of_dfin(),
      new_evt_rcv_fin(0),
      new_evt_rcv_fin(1),
      new_evt_rcv_ack(0),
      new_evt_rcv_ack(1),
      new_evt_rcv_data(),
      new_evt_rcv_dack_of_data(),
      new_evt_snd_dfin(0),
      new_evt_snd_dfin(1),
      new_evt_snd_dack_of_dfin(0),
      new_evt_snd_dack_of_dfin(1),
      new_evt_snd_fin(0),
      new_evt_snd_fin(1),
      new_evt_snd_ack(0),
      new_evt_snd_ack(1),
      new_evt_remote_close(),
      new_evt_remote_snd_data(),
      new_evt_snd_dack_of_data(),
      new_evt_snd_data()
  };

  struct trace a_tr_trace;
  TAILQ_INIT(&a_tr_trace);
  int ret;
  printf("\n");

  while(ret != TEST_DONE)
  {
    init_sf_handshake(f->sf[0]);
    new_sf_handshake(f->sf[1]);

    s = (struct state){.established_sut_ts_sf_count = 2};

    ret = explore_trace(
        token_ct_2sf, sizeof(token_ct_2sf),
        end_trace_cond_2sf,
        &a_tr_trace, 1);

    if(ret == TEST_STOP || ret == TEST_DONE)
    {
      b_flow_fastclose();
    }
  }

  test_case_free();
}
