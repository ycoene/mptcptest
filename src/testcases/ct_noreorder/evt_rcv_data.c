/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "test_ct_noreorder.h"

static int enabled(token_t *this)
{
  err_str = "Data came for no reason";
  return s.cmd_send_data_sent;
}

static void on_set(token_t *this)
{
  s.data_recvd = 1;
  s.data_recvd_sf[this->h.in_sf->id] = 1;
  f->rcv_nxt += 1;
}

static int must_arrive(token_t *this)
{
  return s.cmd_send_data_sent && !s.data_recvd;
}

token_t *new_evt_rcv_data()
{
  token_t *evt = malloc(sizeof(token_t));
  *evt = (token_t) {
    .type = PASSIVE,
    .to_str = "rD",
    .enabled = enabled,
    .on_set = on_set,
    .must_arrive = must_arrive,
    .h = {.f = c_recv_dsn, .args = &s.expected_data_dsn}
  };
  return evt;
};
