/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "config.h"
#include <error.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

int parse_list(char *src, char *dest[], const char *delim, int n)
{
  char *tok, *str = src;
  int i = 0;
  while((tok = strtok(str, delim)) != NULL && i < MAX_HOST_ADDR)
  {
    dest[i] = tok;
    str = NULL;
    i++;
  }
  return i;
}

int load_config()
{
  bzero(&cfg, sizeof(cfg));
  cfg.d = iniparser_load("config/mptcptest.ini");

  char *loc_addr = iniparser_getstring(cfg.d, "net:loc_addr", NULL);
  if(parse_list(loc_addr, cfg.loc_addr, ", ", MAX_HOST_ADDR) == MAX_HOST_ADDR)
    error(EXIT_FAILURE, 0, "Too many addresses declared");

  char *rem_addr = iniparser_getstring(cfg.d, "net:rem_addr", NULL);
  if(parse_list(rem_addr, cfg.rem_addr, ", ", MAX_HOST_ADDR) == MAX_HOST_ADDR)
    error(EXIT_FAILURE, 0, "Too many addresses declared");

  return 0;
}
