/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "packet_handler.h"
#include "testcases.h"

void b_flow_fastclose()
{
  if(f->state==CLOSED)
    return; // already closed

  mp_fastclose_net mpfcn = mp_fastclose_enc(f->rkey);

  handler_t h_rst = {.f=c_recv_rst, .sf=f->asf};
  SUSPEND_PACKET_TREATMENT();
  register_handler(&h_rst);
  send_tcp_seg(f->asf, NULL, (u_char *) &mpfcn, sizeof(mp_fastclose_net), NULL, 0);
  RESUME_PACKET_TREATMENT();

  subflow *active_sf = f->asf;
  int i;

  // reset all remaining subflows
  for(i=0; i<f->nsf; i++)
  {
    if(f->sf[i] != active_sf && f->sf[i]->state != CLOSED)
    {
      b_sf_send_rst(f->sf[i]);
    }
  }

  T_ASSERT(recv_packet(&h_rst)==TEST_CONT, "Fast Close Last RST");

  f->asf = active_sf;

  f->state = CLOSED;
  for(i=0; i<f->nsf; i++)
    f->sf[i]->state = CLOSED;
}

int c_recv_data_fin(C_ARGS) {

  T_ASSERT_RET_ON_FAIL(packet!=NULL, "DATA_FIN sent on close", TEST_STOP);

  tcp_hdr_net *thn = get_tcp_hdr(packet);
  dss_net *dssn = (dss_net *)get_tcp_opt(TCP_OPT_MPTCP, MPTCP_SUBTYPE_DSS, thn);
  if(dssn==NULL) return TEST_IRRELEVANT_PKT;
  dss_loc dssl = dss_dec(dssn);
  if(!(dssl.flags & DSS_F)) return TEST_IRRELEVANT_PKT;

  tcp_hdr_loc thl = tcp_hdr_dec(thn);
  int payload_len = hdr->caplen-((void *)thn-(void *)packet)-thl.doff*4;

  if(payload_len == 0) {
    T_ASSERT(dssl.ssn==0, "DATA_FIN without data");
    T_ASSERT(dssl.dll==1, "DATA_FIN without data");
  }

  /* update flow state */
  if(dssl.dsn == f->rcv_nxt)
    f->rcv_nxt += 1;
  if(thn->flags & TH_FIN)
  {
    f->ct_combined_data_sf_fin_sut_ts = in_sf;
    int i, outstanding_sut_ts_data_other_sf = 0;
    for(i=0; i<f->nsf; i++)
    {
      if(f->sf[i]!=in_sf)
        outstanding_sut_ts_data_other_sf |= f->sf[i]->outstanding_sut_ts_data;
    }
    T_ASSERT(!outstanding_sut_ts_data_other_sf, "Combined DATA_FIN and subflow FIN");
  }

  return TEST_CONT;
}

int c_recv_fin(C_ARGS)
{
  if(packet != NULL)
  { /* check for unexpected packet */
    if(!(get_tcp_hdr(packet)->flags & TH_FIN))
      return TEST_IRRELEVANT_PKT;
  }

  T_ASSERT_RET_ON_FAIL(packet!=NULL, "Subflows termination", TEST_STOP);

  return TEST_CONT;
}

void b_send_fin(int dss_set) {
  subflow *sf = f->asf;

  tcp_hdr_loc hdr = tcp_hdr_auto(sf, TH_FIN|TH_ACK);
  if(dss_set)
  {
    dss_loc dssl = dss_autofill(4, 0, NULL, 0, 0, 0);
    dss_net dssn = dss_enc(&dssl);
    send_tcp_seg(sf, &hdr, (u_char *) &dssn, sizeof(dssn), NULL, 0);
  }
  else
    send_tcp_seg(sf, &hdr, NULL, 0, NULL, 0);

  sf->snd_nxt += 1;
}

void ct_register_sf_fins(handler_t fin[])
{
  int i;
  for(i=0; i<f->nsf; i++)
  {
    if(f->sf[i] != f->ct_combined_data_sf_fin_sut_ts)
    {
      fin[i] = (handler_t){.f=c_recv_fin, .sf=f->sf[i]};
      register_handler(&fin[i]);
    }
  }
}

void ct_register_dack(handler_t *dack, struct c_recv_dack_arg *dargs)
{
  *dargs = (struct c_recv_dack_arg){.dack=f->snd_nxt};
  *dack = (handler_t){.f=c_recv_dack, .copy_packet=1, .args=dargs};
  register_handler(dack);
}

int ct_recv_sf_fins(handler_t fin[], handler_t ack[])
{
  int i, ret;
  for(i=0; i<f->nsf; i++)
  {
    f->asf = f->sf[i];
    /* Receive subflow FIN */
    if(f->sf[i] != f->ct_combined_data_sf_fin_sut_ts)
    {
      T_CHECK(recv_packet(&fin[i]));
    }
    dss_loc dssl = dss_autofill(4, 0, NULL, 0, 0, 0);

    SUSPEND_PACKET_TREATMENT();
    ack[i] = (handler_t){ .f=c_recv_ack, .sf=f->sf[i]};
    register_handler(&ack[i]);
    /* Send subflow FIN */
    b_send_fin(1);
    b_send_data(NULL, &dssl, NULL, 0, TH_FIN|TH_ACK);
    RESUME_PACKET_TREATMENT();
  }

  /* Receive last ACKs */
  for(i=0; i<f->nsf; i++)
  {
    T_CHECK((ret = recv_packet(&ack[i])));
    f->sf[i]->state = CLOSED;
  }

  cleanup:
  return ret;
}

void ct_send_data_finack()
{
  u_char data[] = {'0'};
  dss_loc dssl;

  // send DATA_FIN + DATA_ACK[DATA_FIN]
  if(f->ct_combined_fin_ack)
  {
    dssl = (dss_loc) {
      .dack_len = 4,
      .dack = f->rcv_nxt,
      .dsn_len = 4,
      .dsn = f->snd_nxt,
      .dll = 1,
      .flags = DSS_F
    };
    dss_do_checksum(&dssl, NULL);
  }
  else
  {
    // send DATA_ACK[DATA_FIN]
    dss_loc dssl = dss_autofill(4, 0, NULL, 0, 0, 0);
    b_send_data(NULL, &dssl, NULL, 0, TH_ACK);

    // send DATA_FIN
    dssl = (dss_loc) {
      .dsn_len = 4,
      .dsn = f->snd_nxt,
      .dll = 1,
      .flags = DSS_F
    };
    dss_do_checksum(&dssl, NULL);
  }

  if(f->ct_combined_fin_data)
  {
    b_send_data(NULL, &dssl, data, sizeof(data), TH_ACK);
  }
  else
  {
    b_send_data(NULL, &dssl, NULL, 0, TH_ACK);
  }
}

int t_fin_exchange_sut_ts_1()
{
  int retval = TEST_STOP;
  handler_t fin[MAX_NB_SF], ack[MAX_NB_SF], dack={};
  struct c_recv_dack_arg dack_args;

  // receive DATA_FIN
  T_CHECK(send_recv(b_remote_send_data_fin, &(handler_t){.f=c_recv_data_fin}));

  SUSPEND_PACKET_TREATMENT();
  ct_register_sf_fins(fin);
  // send DATA FIN+ACK
  ct_send_data_finack();
  ct_register_dack(&dack, &dack_args);
  RESUME_PACKET_TREATMENT();

  // close remaining subflows
  T_CHECK(ct_recv_sf_fins(fin, ack));

  // receive DATA_ACK[DATA_FIN]
  T_CHECK(recv_packet(&dack));

  f->state = CLOSED;
  retval = TEST_CONT;

  cleanup:
  if(dack.packet!=NULL) free(dack.packet);
  return retval;
}

int t_fin_exchange_sut_ts_2()
{
  handler_t fin[MAX_NB_SF], ack[MAX_NB_SF], dack;

  uint64_t rcv_nxt_before = f->rcv_nxt;
  // receive DATA_FIN
  T_CHECK(send_recv(b_remote_send_data_fin, &(handler_t){.f=c_recv_data_fin}));
  uint64_t rcv_nxt_after = f->rcv_nxt;
  f->rcv_nxt = rcv_nxt_before;// pretend not having received the last fin

  SUSPEND_PACKET_TREATMENT();
  dack =(handler_t){.f=c_recv_dack};
  register_handler(&dack);
  // send DATA_FIN
  ct_send_data_finack();
  RESUME_PACKET_TREATMENT();

  // receive DATA_ACK
  recv_packet(&dack);

  SUSPEND_PACKET_TREATMENT();
  ct_register_sf_fins(fin);

  // send DATA_ACK[DATA_FIN]
  f->rcv_nxt = rcv_nxt_after;
  dss_loc dssl = dss_autofill(4, 0, NULL, 0, 0, 0);
  b_send_data(NULL, &dssl, NULL, 0, TH_ACK);
  RESUME_PACKET_TREATMENT();

  T_CHECK(ct_recv_sf_fins(fin, ack));

  f->state = CLOSED;

  return TEST_CONT;

  cleanup: return TEST_STOP;
}

int t_data_exchange_ts_sut_1()
{
  struct c_recv_dack_arg args;
  handler_t h = {.f=c_recv_dack, .args=&args};

  u_char data[] = {'t','e','s','t'};
  u_char data2[] = {'t','e','s','t','0'}; // the last byte will be sent with the DFIN
  dss_loc dssl;

  if(f->ct_combined_fin_data)
    dssl = dss_autofill(4, 4, data2, sizeof(data2), 1, 0);
  else
    dssl = dss_autofill(4, 4, data, sizeof(data), 1, 0);

  SUSPEND_PACKET_TREATMENT();
  if(!f->ct_combined_fin_data)
    register_handler(&h);
  b_send_data(NULL, &dssl, data, sizeof(data), TH_ACK);
  args = (struct c_recv_dack_arg){.dack=f->snd_nxt};
  RESUME_PACKET_TREATMENT();

  if(!f->ct_combined_fin_data)
    T_CHECK(recv_packet(&h)); // recv DATA_ACK

  return TEST_CONT;
  cleanup: return TEST_STOP;
}

int t_data_exchange_sut_ts_1()
{
  struct c_recv_dss_arg a = {.dack_len=DACKANY, .dsn_len=DSNANY};
  handler_t h = {.f=c_recv_dss, .args=&a};
  register_handler(&h);
  b_remote_send_data(1, 0);
  recv_packet(&h);
  if(!f->asf->outstanding_sut_ts_data)
  {
    f->rcv_nxt++;
    dss_loc dssl = dss_autofill(4, 0, NULL, 0, 0, 0);
    b_send_data(NULL, &dssl, NULL, 0, TH_ACK);
  }

  return TEST_CONT;
}

void ts_close_sut_ts()
{
  test_case_init(2);

  int cmb_fin_ack[] = {1};
  int fin_ex_path[] = {1};
  int cmb_fin_data[] = {0};

  int i, j, k, l, m;

  ARRAY_FOREACH_IDX(m, cmb_fin_data)
  ARRAY_FOREACH_IDX(l, cmb_fin_ack)
  ARRAY_FOREACH_IDX(k, fin_ex_path)
  for(j=0;j<1;j++)
  {
    if(m==1 && (l!=0 || k!=0)) continue;

    f->ct_combined_fin_ack = cmb_fin_ack[l];
    f->ct_combined_fin_data = cmb_fin_data[m];
    f->ct_combined_data_sf_fin_sut_ts = 0;

    f->asf = f->sf[0];
    T_CHECK(init_sf_handshake(NULL));

    for(i=1; i<f->nsf; i++)
    {
      f->asf = f->sf[i];
      T_CHECK(new_sf_handshake(NULL));
    }

    T_CHECK(t_data_exchange_ts_sut_1());
    T_CHECK(t_data_exchange_sut_ts_1());

    switch(fin_ex_path[k])
    {
      case 0: T_CHECK(t_fin_exchange_sut_ts_1()); break;
      case 1: T_CHECK(t_fin_exchange_sut_ts_2()); break;
    }

    continue;
    cleanup:
    b_flow_fastclose();
  }

  test_case_free();
}
