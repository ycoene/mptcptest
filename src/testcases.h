/**
 * @file      testcases.h
 * @brief     MPTCP Test Cases
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#ifndef TESTCASES_H_
#define TESTCASES_H_

#include "alternative.h"
#include "config.h"
#include "mptcp.h"
#include "packet_handler.h"
#include "callback_args.h"
#include "server.h"
#include "tcp.h"
#include <CUnit/CUnit.h>
#include <openssl/hmac.h>
#include <openssl/sha.h>
#include <pthread.h>
#include <string.h>
#include <time.h>

#define C_ARGS void *args, const struct pcap_pkthdr *hdr, const u_char *packet, subflow *in_sf
#define C_ARGS_INST args, hdr, packet, in_sf

/* Test return values */

#define TEST_PASS_CONT  0x0
#define TEST_PASS_STOP  0x1
#define TEST_FAIL_CONT  0x2
#define TEST_FAIL_STOP  0x3

#define TEST_CONT       0x4
#define TEST_STOP       0x5
#define TEST_IRRELEVANT_PKT  0x6
#define TEST_DONE       0x7

flow *f;

#define ARRAY_FOREACH(eltp, array) \
  for(eltp = &array[0]; eltp != &array[sizeof(array)/sizeof(array[0])]; eltp++)

#define ARRAY_FOREACH_IDX(i, array) \
  for(i=0; i<sizeof(array)/sizeof(array[0]); i++)

#define T_ASSERT(test_exp, msg) ({  \
  int test_val = (test_exp);        \
  if(test_val) { CU_PASS(msg); }    \
  else { CU_FAIL(msg); }            \
  test_val; })

#define T_ASSERT_RET_ON_FAIL(test, msg, retval)     \
  do { if(test) { CU_PASS(msg); }                   \
       else { CU_FAIL(msg); return retval; }}       \
  while(0)

#define T_ASSERT_RET_ON_PASS(test, msg, retval) { \
  {if(test) { CU_PASS(msg); return retval; } else { CU_FAIL(msg); }} \
}

#define T_CHECK(fcall) { \
  if(fcall==TEST_STOP) goto cleanup; \
}

void hash_key_sha1(uint8_t *hash, key64 key);
token32 compute_token(const key64 key);
int remote_get_port();
void b_remote_ifconfig(int status);
int is_set(u_int flag, u_int flags);
void hmac_sha1(uint8_t *dst, const key64 *k1, const key64 *k2, const token32 *r1, const token32 *r2);
int c_recv_rst(C_ARGS);
int new_sf_handshake(subflow *sf);
void b_remote_send_data(int len, int close);
void ts_close_sut_ts_2();
int c_recv_data_fin(C_ARGS);
int c_recv_fin(C_ARGS);
void ts_close_explore_1sf();
void ts_close_explore_2sf();
void b_init_sf_remote_send_syn();
int c_recv_data(C_ARGS);

void test_case_init(int nsf);
void test_case_free();

CF_DEF(int, c_recv_dss, C_ARGS, {
  enum dack_len {CONC(DACK0, DACK4, DACK8, DACKSET, DACKANY)} dack_len;
  enum dsn_len {CONC(DSN0, DSN4, DSN8, DSNSET, DSNANY)} dsn_len;
});

CF_DEF(int, c_recv_ack, C_ARGS, {uint32_t ack;});

void b_send_data(subflow *sf, dss_loc *dssl, u_char *pl, int pl_len, uint flags);


/* Initial subflow establishement */

/** @brief Send initial SYN TS->SUT */
void b_init_sf_send_syn();

/** @brief Check incoming SYN+ACK */
int c_init_sf_recv_synack(C_ARGS);

/** @brief Send initial SYN and check answer */
int t_init_sf_send_syn_recv_synack();

/** @brief Send initial handshake ACK */
void b_init_sf_send_ack();

/** @brief Test connection initiation from TS to TUS */
void ts_init_ts_sut();

/** @brief Test connection initiation from SUT to TS */
void ts_init_sut_ts();

/** @brief Connection init. TS->SUT with wrong 3rd ACK */
void ts_init_wrong_ack();
void ts_init_syn_fuzz();
void ts_dss_fuzz();

int init_sf_handshake(subflow *sf);
int init_sf_handshake_sutts(subflow *sf);

/**
 * Starting a new subflow
 */

/** @brief Send SYN on new subflow */
int t_new_sf_send_syn();

/** @brief Check incoming SYN+ACK on new subflow */
int c_new_sf_recv_synack(C_ARGS);

/** @brief Send SYN on new subflow and check SYN+ACK answer */
int t_new_sf_send_syn_recv_synack();

/** @brief Check incoming fourth ACK of new subflow handshake */
int c_new_sf_recv_4thack(C_ARGS);

/** @brief Send new subflow handshake ACK and check incoming fourth ACK */
int t_new_sf_send_ack_recv_4thack();

/** @brief Test connection initiation */
void ts_new_sf_ts_sut();

void ts_mp_join_b();

void ts_new_sf_sutts();

void ts_fallback_data_without_dss();

/**
 * Other
 */

/** @brief Send RST packet */
int b_sf_send_rst(subflow *sf);

/** @brief Send RST to all subflows */
void b_sf_abort_all();

/** @brief returns TEST_CONT */
int c_nop(C_ARGS);

/*
 * Closing a connection
 */

void ts_close_sut_ts();
void b_remote_send_data_fin();

/*
 * Data exchange
 */

void ts_data_ts_sut();
void ts_data_sut_ts();
void ts_ack_64bit_dsn();

CF_DEF(int, c_recv_dack, C_ARGS, {uint64_t dack;});
CF_DEF(int, c_recv_dsn, C_ARGS, {uint64_t dsn; int dll;});

/*
 * Fast close
 */

void b_flow_fastclose();

#endif /* TESTCASES_H_ */
