/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "tcp.h"
#include <stdlib.h>

tcp_hdr_loc tcp_hdr_dec(const tcp_hdr_net *thn) {
  tcp_hdr_loc thl = {
      .sp = ntohs(thn->sport),
      .dp = ntohs(thn->dport),
      .seq = ntohl(thn->seq),
      .ack = ntohl(thn->ack),
      .doff = (thn->doffres)>>4,
      .flags = thn->flags,
      .win = ntohs(thn->window),
      .checksum = thn->checksum,
      .urgent = thn->urgent
  };
  return thl;
}

tcp_hdr_net *get_tcp_hdr(const u_char *frame)
{
  const u_char *ip_hdr = frame + SIZE_ETHER;
  int ip_hdr_len = (*(ip_hdr) & 0x0F) * 4; // IHL field value (#words) * 4 (|word|)
  const u_char *tcp_hdr = ip_hdr + ip_hdr_len;
  return (tcp_hdr_net *) tcp_hdr;
}

u_char * get_tcp_opt(int type, int mptcp_subtype, const tcp_hdr_net *hdr)
{
  const u_char *tcp_opt = (u_char *)hdr+SIZE_TCP_HDR_BASE;
  int tcp_hdr_len = 4*((hdr->doffres)>>4);
  int offset = 0;
  int length;
  /* search among TCP options */
  /* TODO fix: may segfault for invalid data offset value */
  while(offset < tcp_hdr_len-SIZE_TCP_HDR_BASE)
  {
    int curr_type = *(tcp_opt+offset);
    if(curr_type == TCP_OPT_NOP)
      length = 1;
    else
      length = *(tcp_opt+offset+1);
    if(curr_type == type)
    {
      if(type == TCP_OPT_MPTCP)
      {
        int curr_subtype = *(tcp_opt+offset+2)>>4;
        if(curr_subtype == mptcp_subtype)
          return (u_char *)(tcp_opt+offset);
      }
      else
      {
        return (u_char *)(tcp_opt+offset);
      }
    }
    offset += length;
  }
  return NULL;
}

int payload_len(const struct pcap_pkthdr *hdr, const u_char *packet)
{
  tcp_hdr_net *thn = get_tcp_hdr(packet);
  tcp_hdr_loc thl = tcp_hdr_dec(thn);
  return hdr->caplen-((void *)thn-(void *)packet)-thl.doff*4;
}

tcp_hdr_loc tcp_hdr_auto(subflow *sf, uint8_t flags)
{
  return (tcp_hdr_loc) {
    .sp = sf->lport,
    .dp = sf->rport,
    .seq = sf->snd_nxt,
    .ack = (flags & TH_ACK)? sf->rcv_nxt: 0,
    .flags = flags,
    .win = sf->rcv_wnd,
  };
}

int send_tcp_seg(subflow *sf, tcp_hdr_loc *hdr, u_char *opt, int opt_s, u_char *pl, int pl_s)
{
  tcp_hdr_loc hdr_auto;
  if(hdr == NULL)
  {
    hdr_auto = tcp_hdr_auto(sf, TH_ACK);
    hdr = &hdr_auto;
  }

#ifdef __x86_64__
  libnet_clear_packet(sf->l);
  sf->ptag_tcp_opt = sf->ptag_tcp = sf->ptag_ip = 0;
#endif

  if((sf->ptag_tcp_opt = libnet_build_tcp_options(opt, opt_s, sf->l, sf->ptag_tcp_opt)) == -1)
  {
    fprintf(stderr, "Error building TCP options: %s\n", libnet_geterror(sf->l));
    exit(EXIT_FAILURE);
  }

  if((sf->ptag_tcp = libnet_build_tcp(hdr->sp, hdr->dp, hdr->seq, hdr->ack,
      hdr->flags, hdr->win, 0, 0, LIBNET_TCP_H + opt_s + pl_s, pl, pl_s, sf->l, sf->ptag_tcp)) == -1)
  {
    fprintf(stderr, "Error building TCP header: %s\n", libnet_geterror(sf->l));
    exit(EXIT_FAILURE);
  }

  if((sf->ptag_ip = libnet_build_ipv4(LIBNET_IPV4_H + LIBNET_TCP_H + opt_s + pl_s, 0, 0, 0, 64,
      IPPROTO_TCP, 0, sf->laddr, sf->raddr, NULL, 0, sf->l, sf->ptag_ip)) == -1)
  {
    fprintf(stderr, "Error building IP header: %s\n", libnet_geterror(sf->l));
    exit(EXIT_FAILURE);
  }

  int retval;
  if((retval = libnet_write(sf->l)) == -1)
  {
    fprintf(stderr, "Error writing packet: %s\n", libnet_geterror(sf->l));
    exit(EXIT_FAILURE);
  }

  return retval;
}
