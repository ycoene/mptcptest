/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "testcases.h"

/**
 * Test that a first data packet without DSS provokes a fallback to
 * regular TCP.
 */
void ts_fallback_data_without_dss()
{
  test_case_init(1);
  T_CHECK(init_sf_handshake(f->sf[0]));

  handler_t ack = {
      .f = c_recv_ack,
      .args = &(CF_ARG_TYPE(c_recv_ack)) {.ack = f->sf[0]->snd_nxt + 1},
      .copy_packet = 1,
  };
  register_handler(&ack);

  u_char map[1];
  b_send_data(f->sf[0], NULL, map, sizeof(map), TH_ACK);
  T_CHECK(recv_packet(&ack));

  dss_net *dssn = (dss_net *) get_tcp_opt(TCP_OPT_MPTCP, MPTCP_SUBTYPE_DSS,
      get_tcp_hdr(ack.packet));
  T_ASSERT(dssn == NULL, "First data segment without DSS");

  cleanup:
  free(ack.packet);
  b_sf_send_rst(f->sf[0]);
  test_case_free();
}
