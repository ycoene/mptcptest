/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "testcases.h"

/**
 * Trigger the SUT to initiate a new test connection.
 */
void b_init_sf_remote_send_syn()
{
  f->asf->state = LISTEN;
  char buf[] = {CMD_INIT};
  write(cmd_sock, buf, sizeof(buf));
}

/**
 * Trigger the SUT to send data and possibly close the test connection
 * afterwards.
 * @param len amount of data to send
 * @param close 0: do nothing after data transfer
 *              1: call close() on the test socket
 *              2: call shutdown(SHUT_WR) on the test socket
 */
void b_remote_send_data(int len, int close)
{
  char buf[6];
  buf[0] = CMD_SEND;
  *((uint32_t *)&buf[1]) = htobe32(len);
  *((uint8_t *)&buf[5]) = (uint8_t)close;
  write(cmd_sock, buf, sizeof(buf));
}

void b_remote_send_data_fin() {
  char buf[] = {CMD_CLOSE};
  write(cmd_sock, buf, sizeof(buf));
}

/**
 * @return port the SUT is listening to for test connections
 */
int remote_get_port()
{
  uint16_t ans;
  char cmd = CMD_PORT;
  write(cmd_sock, &cmd, 1);
  if(read(cmd_sock, &ans, sizeof(ans))!=sizeof(ans))
    printf("ERROR on remote_get_port\n");
  return ntohs(ans);
}

/**
 * Enable or disable a network interface on the SUT.
 * @param status 1: enable or 0: disable
 */
void b_remote_ifconfig(int status)
{
  char buf[2] = {CMD_IFCFG, (char)status};
  write(cmd_sock, buf, sizeof(buf));
}
