/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include <CUnit/CUnit.h>
#include <sys/socket.h>
#include <sys/types.h>
#include "barrier.h"
#include "config.h"
#include "CUnit/Basic.h"
#include "CUnit/Console.h"
#include "mptcp.h"
#include "packet_handler.h"
#include "server.h"
#include "testcases.h"

int main(int argc, char* argv[]) {

  load_config();

  if(pthread_mutex_init(&pcap_callback_mutex, NULL)!=0) {
    perror("ERROR on pthread_mutex_init");
  }

  cmd_sock = socket(AF_INET, SOCK_STREAM, 0); // init command socket
  struct sockaddr_in sin = {
      .sin_family = AF_INET,
      .sin_port = htons(PORT_CMD),
      .sin_addr.s_addr = libnet_name2addr4(NULL, cfg.rem_addr[0], LIBNET_RESOLVE)
  };

  if(connect(cmd_sock, (struct sockaddr *) &sin, sizeof(sin)) < 0)
  {
    perror("ERROR on connect");
    exit(EXIT_FAILURE);
  }

  /* Set up CUnit tests */

  if(CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  CU_pSuite pSuite = CU_add_suite("mptcptest", NULL, NULL);
  if(NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  if( (NULL == CU_ADD_TEST(pSuite, ts_init_ts_sut))                 ||
      (NULL == CU_ADD_TEST(pSuite, ts_init_wrong_ack))              ||
      (NULL == CU_ADD_TEST(pSuite, ts_init_sut_ts))                 ||
      (NULL == CU_ADD_TEST(pSuite, ts_new_sf_ts_sut))               ||
      (NULL == CU_ADD_TEST(pSuite, ts_new_sf_sutts))                ||
      (NULL == CU_ADD_TEST(pSuite, ts_mp_join_b))                   ||
      (NULL == CU_ADD_TEST(pSuite, ts_close_sut_ts))                ||
      (NULL == CU_ADD_TEST(pSuite, ts_close_sut_ts_2))              ||
      (NULL == CU_ADD_TEST(pSuite, ts_close_explore_1sf))           ||
      (NULL == CU_ADD_TEST(pSuite, ts_close_explore_2sf))           ||
      (NULL == CU_ADD_TEST(pSuite, ts_data_ts_sut))                 ||
      (NULL == CU_ADD_TEST(pSuite, ts_data_sut_ts))                 ||
      (NULL == CU_ADD_TEST(pSuite, ts_ack_64bit_dsn))               ||
      (NULL == CU_ADD_TEST(pSuite, ts_init_syn_fuzz))               ||
      (NULL == CU_ADD_TEST(pSuite, ts_dss_fuzz))                    ||
      (NULL == CU_ADD_TEST(pSuite, ts_fallback_data_without_dss)))
  {
    CU_cleanup_registry();
    return CU_get_error();
  }

//  CU_basic_set_mode(CU_BRM_VERBOSE);
//  CU_basic_run_tests();

  CU_console_run_tests();
//  CU_curses_run_tests();

  CU_cleanup_registry();

  close(cmd_sock);
  iniparser_freedict(cfg.d);

  return CU_get_error();
}
