/**
 * @file      tcp.h
 * @brief     TCP-related structures and functions.
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#ifndef TCP_H_
#define TCP_H_

#include "mptcp.h"

#define SIZE_TCP_HDR_BASE 20
#define SIZE_ETHER 14

// http://www.tcpdump.org/pcap.htm
typedef struct {
  u_char ip_vhl;            /* version << 4 | header length >> 2 */
  u_char ip_tos;            /* type of service */
  u_short ip_len;           /* total length */
  u_short ip_id;            /* identification */
  u_short ip_off;           /* fragment offset field */
#define IP_RF 0x8000        /* reserved fragment flag */
#define IP_DF 0x4000        /* dont fragment flag */
#define IP_MF 0x2000        /* more fragments flag */
#define IP_OFFMASK 0x1fff   /* mask for fragmenting bits */
  u_char ip_ttl;            /* time to live */
  u_char ip_p;              /* protocol */
  u_short ip_sum;           /* checksum */
  u_int32_t ip_src, ip_dst; /* source and dest address */
} ip_hdr_net;

/**
 * TCP header. Network representation.
 */
typedef struct tcp_hdr_net {
  uint16_t sport;
  uint16_t dport;
  uint32_t seq;
  uint32_t ack;
  uint8_t doffres;
  uint8_t flags;
  uint16_t window;
  uint16_t checksum;
  uint16_t urgent;
} tcp_hdr_net;

/**
 * TCP header. Local representation.
 */
typedef struct tcp_hdr_loc {
  uint16_t sp;
  uint16_t dp;
  uint32_t seq;
  uint32_t ack;
  uint8_t doff;
  uint8_t flags;
  uint16_t win;
  uint16_t checksum;
  uint16_t urgent;
} tcp_hdr_loc;

/**
 * Window scale option.
 */
typedef struct tcp_opt_ws {
  uint8_t kind;
  uint8_t length;
  uint8_t shift_cnt;
} tcp_opt_ws;

tcp_hdr_loc tcp_hdr_dec(const tcp_hdr_net *thn);

/**
 * @param frame pointer to an Ethernet frame
 * @return pointer to the TCP header of \c frame
 */
tcp_hdr_net *get_tcp_hdr(const u_char *frame);

/**
 * Find an option within a TCP segment.
 * @param type option type
 * @param mptcp_subtype MPTCP subtype (if type is TCP_OPT_MPTCP)
 * @param hdr TCP header
 * @return
 */
u_char *get_tcp_opt(int type, int mptcp_subtype, const tcp_hdr_net *hdr);

int payload_len(const struct pcap_pkthdr *hdr, const u_char *packet);

/**
 * @return a local representation of a TCP header filled according to the
 * current state of subflow \c sf and with \c flags flags set.
 */
tcp_hdr_loc tcp_hdr_auto(subflow *sf, uint8_t flags);

/**
 * Send a TCP segment.
 */
int send_tcp_seg(subflow *sf, tcp_hdr_loc *hdr, u_char *opt, int opt_s, u_char *pl, int pl_s);

/**
 * TCP segment representation
 */
typedef struct {
  subflow *sf;
  tcp_hdr_loc hdr;
  u_char opt[64];
  uint opt_s;
  u_char *payload;
  uint payload_s;
} mptcp_seg;

#endif /* TCP_H_ */
