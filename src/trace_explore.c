/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "testcases.h"
#include "trace_explore.h"
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#define container_of(ptr, type, member) ({ \
                const typeof( ((type *)0)->member ) *__mptr = (ptr); \
                (type *)( (char *)__mptr - offsetof(type,member) );})

#define FCALL_IFDEF(f, args) (f == NULL? 0: f(args))

/**
 * State transition triggered by receiving token \c taken
 */
int follow_passive_tr(token_t *taken)
{
  if(taken != NULL)
  {
    recv_packet(&taken->h);
    printf("(%s) ", taken->to_str);
    if(FCALL_IFDEF(taken->enabled, taken))
    {
      FCALL_IFDEF(taken->on_set, taken);
    }
    else
    {
      printf("\nct: %s", err_str);
      CU_FAIL("Forbidden transition taken");
      return TEST_STOP;
    }
    free(taken->h.packet);
  }
  return TEST_CONT;
}

// Global arguments of sigaction handler
struct handler_set *hs_ptr;
int timeout = 0;

/**
 * Token timeout
 */
void sa_handler1(int sig)
{
  timeout = 1;
  pthread_cond_broadcast(&hs_ptr->cond);
}

/**
 * Wait for a determined token tok to occur (passive transition).
 */
int wait_for_passive_tr(token_t *tok, struct handler_set *hs)
{
  handler_t *h;
  token_t *taken;
  int retval = TEST_CONT;

  // TODO maybe unregister handler in p_tr_taken
  printf("w[%s] ", tok->to_str);
  alarm(TIMEOUT);
  // wait until reception of token that must come
  while((taken = (h=handler_select(hs, 0))==NULL? NULL: container_of(h, token_t, h)) != tok)
  {
    T_CHECK((retval = follow_passive_tr(taken)));

    if(timeout)
    {
      printf("\nct: %s timed out", tok->to_str);
      CU_FAIL("Expected packet never arrived");
      retval = TEST_STOP;
      break;
    }
  }

  cleanup:
  alarm(0); // expected packet arrived, call off timer
  timeout = 0;
  return retval;
}

int explore_trace(token_t *tok_array[], uint tok_array_s, int (*end_trace_cond)(),
    struct trace *trace, int random)
{
  int i, retval = TEST_CONT;

  // build token list
  LIST_HEAD(tok_list, token) tok_list;
  LIST_INIT(&tok_list);

  struct handler_set hs;
  sigaction(SIGALRM, &(struct sigaction){.sa_handler=sa_handler1}, NULL);

  handler_set_init(&hs);
  hs_ptr = &hs;

  for(i=tok_array_s/sizeof(tok_array[0])-1; i>=0; i--)
  {
    LIST_INSERT_HEAD(&tok_list, tok_array[i], entries);
    if(tok_array[i]->type == PASSIVE)
    {
      tok_array[i]->h.reusable = 1;
      register_handler(&tok_array[i]->h);
      handler_set(&tok_array[i]->h, &hs);
    }
  }

  trace_node *tn = trace->tqh_first, *last_assigned_trace_elt = NULL;

  // take_first_path is set when it appears that all trace suffixes of
  // the current trace prefix have already been explored.
  int take_first_path = 0;

  // compute next trace of active transitions
  while(!(end_trace_cond()))
  {
    // add a node for next element of trace
    if(tn == NULL)
    {
      tn = (trace_node *) malloc(sizeof(trace_node));
      bzero(tn, sizeof(trace_node));

      if(trace->tqh_first == NULL)
        TAILQ_INSERT_HEAD(trace, tn, entries);
      else
        TAILQ_INSERT_TAIL(trace, tn, entries);
    }

    token_t *tok;

    if(!random)
    {
      // determine which active transition (or wait) will be tried next for this node
      if(tn->tok == NULL)
      {
        tok = tok_list.lh_first;
      }
      else if(take_first_path)
      {
        tok = tok_list.lh_first;
        tn->exhausted = 0;
      }
      else if(tn->exhausted)
      {
        printf("[X]");
        tok = tn->tok->entries.le_next;
        tn->exhausted = 0;
        take_first_path = 1;
      }
      else
      {
        tok = tn->tok;
      }
    }

    // follow taken passive transitions
    handler_t *h;
    while((h = handler_select(&hs, 1)) != NULL)
    {
      token_t *taken = container_of(h, token_t, h);
      T_CHECK((retval = follow_passive_tr(taken)));
    }

    if(!random) // determine next enabled active transition
    {
      for(; tok != NULL; tok = tok->entries.le_next)
      {
        if((tok->type == ACTIVE && tok->enabled(tok))
            || FCALL_IFDEF(tok->must_arrive, tok))
        {
          break;
        }
      }
    }
    else // randomly choose among enabled transitions
    {
      token_t *enabled[32]; int i = 0;

      LIST_FOREACH(tok, &tok_list, entries)
      {
        if((tok->type == ACTIVE && tok->enabled(tok))
            || FCALL_IFDEF(tok->must_arrive, tok))
          enabled[i++] = tok;
      }
      tok = (i==0)? NULL: enabled[rand()%i];
    }

    // take transition
    if(tok != NULL)
    {
      tn->tok = tok;
      last_assigned_trace_elt = tn;

      if(tok->type == ACTIVE)
      {
        printf("%s ", tok->to_str);
        FCALL_IFDEF(tok->on_set, tok);
        FCALL_IFDEF(tok->f, tok);
        LIST_REMOVE(tok, entries); // TODO maybe delete this line
      }
      else
      {
        T_CHECK((retval = wait_for_passive_tr(tok, &hs)));
      }
      tn = tn->entries.tqe_next;
    }
    else // no transition possible
    {
      if(tn != trace->tqh_first) // mark previous trace element as exhausted
      {
        TAILQ_PREV(tn, trace, entries)->exhausted = 1;
        take_first_path = 1;
      }
      else // DFS completed
      {
        retval = TEST_DONE;
        goto cleanup;
      }
    }
  }

  cleanup: printf("\n");

  // unregister and reset handlers
  for(i=0; i<tok_array_s/sizeof(tok_array[0]); i++)
  {
    if(tok_array[i]->type == PASSIVE)
    {
      unregister_handler(&tok_array[i]->h);
    }
  }
  handler_set_destroy(&hs);

  // we will (try) not (to) take the same path next time
  if(last_assigned_trace_elt != NULL)
  {
    last_assigned_trace_elt->exhausted = 1;
  }
  return retval;
}
