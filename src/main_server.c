/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include <fcntl.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include "server.h"

#define TEST_PORT_START 1024
#define TEST_PORT_END   65535

#define SOCK_FREE   -1

#define max(x,y) ((x) > (y) ? (x) : (y))

#define DATA_BUF_SIZE 1042441

int cmd_sock_listen, cmd_sock_cl;
int test_sock_listen, test_sock_cl = SOCK_FREE;

char send_data_buf[DATA_BUF_SIZE];

int main(int argc, char *argv[])
{
  // initialize send data buffer with arbitrary data
  int i;
  for(i=0; i<DATA_BUF_SIZE; i++) send_data_buf[i] = (char)i;

  if((cmd_sock_listen = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    perror("ERROR opening socket");

  if((test_sock_listen = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    perror("ERROR opening socket");

  int last_port_fd = open("./last_test_port", O_RDWR);
  if (last_port_fd < 0) {
	  printf("You should launch the server in the mptcptest-directory.\n");
	  printf("Also, please check that the file last_test_port is readable\n");
	  return 0;
  }
  int *last_port = (int *)mmap(NULL, sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED, last_port_fd, 0);
  (*last_port)++;

  struct sockaddr_in listen_addr_cmd = {
    .sin_family = AF_INET,
    .sin_addr.s_addr = INADDR_ANY,
    .sin_port = htons(PORT_CMD)
  };

  struct sockaddr_in listen_addr_test = {
    .sin_family = AF_INET,
    .sin_addr.s_addr = INADDR_ANY,
    .sin_port = htons(*last_port)
  };

  int optval = 1;
  setsockopt(cmd_sock_listen, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);
  setsockopt(test_sock_listen, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

  if(bind(cmd_sock_listen, (struct sockaddr *) &listen_addr_cmd,
      sizeof(struct sockaddr_in)) < 0)
    perror("ERROR on binding");

  if(bind(test_sock_listen, (struct sockaddr *) &listen_addr_test,
      sizeof(struct sockaddr_in)) < 0)
    perror("ERROR on binding");

  printf("listening for test connection on port %i\n", *last_port);

  listen(cmd_sock_listen, 5);
  listen(test_sock_listen, 5);

  struct sockaddr_in cl_addr_cmd;
  socklen_t cl_addr_len_cmd = sizeof(cl_addr_cmd);

  cmd_sock_cl = accept(cmd_sock_listen, (struct sockaddr *) &cl_addr_cmd, &cl_addr_len_cmd);

  if(cmd_sock_cl < 0)
    perror("ERROR on accept");
  else
    printf("command connection established\n");

  fd_set rfds;
  FD_ZERO(&rfds);
  FD_SET(test_sock_listen, &rfds);
  FD_SET(cmd_sock_cl, &rfds);

  int retval, nfds = max(test_sock_listen, cmd_sock_cl)+1;

  struct sockaddr_in cl_addr_test;
  socklen_t cl_addr_test_len = sizeof(cl_addr_test);

  while(1)
  {
    retval = select(nfds, &rfds, NULL, NULL, NULL);

    int test_conn_closed = 0, test_conn_remote_closed = 0;
    if(FD_ISSET(test_sock_listen, &rfds))
    {
      printf("accepting incoming connection\n");

      if(test_sock_cl != SOCK_FREE)
        close(test_sock_cl);

      test_sock_cl = accept(test_sock_listen, (struct sockaddr *) &cl_addr_test, &cl_addr_test_len);
      if(test_sock_cl < 0) perror("ERROR on accept");
    }
    else if(test_sock_cl != SOCK_FREE && FD_ISSET(test_sock_cl, &rfds))
    {
      int nread;
      char buf[512];

      nread = read(test_sock_cl, buf, sizeof(buf));
      if(nread <= 0)
      {
        printf("connection closed by remote end\n");
        test_conn_remote_closed = 1;
//        FD_CLR(test_sock_cl, &rfds);
//        close(test_sock_cl);
//        test_sock_cl = SOCK_FREE;
//        test_conn_closed = 1;
      }
      else
      {
        printf("received test data %i bytes\n", nread);
      }
    }
    if(FD_ISSET(cmd_sock_cl, &rfds))
    {
      int nread;
      char cmd;
      uint32_t send_data_len, i, d, r;
      uint16_t last_port_be;
      uint8_t close_after_send;
      uint8_t ifconfig_status;

      if((nread = read(cmd_sock_cl, &cmd, 1)) > 0)
      {
        switch(cmd){
        case CMD_CLOSE:
          printf("command: close connection\n");
          close(test_sock_cl);
          test_sock_cl = SOCK_FREE;
          test_conn_closed = 1;
          break;

        case CMD_INIT:
          printf("command: initiate connection\n");
//          if(test_sock_cl != SOCK_FREE) {
//            printf("a connection already exists\n");
//          }
//          else {
          if((test_sock_cl = socket(AF_INET, SOCK_STREAM, 0)) < 0)
            perror("ERROR opening socket");

          struct sockaddr_in test_srv_addr = {
              .sin_family = AF_INET,
              .sin_addr.s_addr = 0x0101020A,
              .sin_port = htons(PORT_SUT)
          };

          if(connect(test_sock_cl, (struct sockaddr *) &test_srv_addr, sizeof(test_srv_addr)) < 0)
            printf("ERROR on connect: %s\n", strerror(errno));
//          }
          break;

        case CMD_SEND:
          // parse integer
          read(cmd_sock_cl, &send_data_len, 4);
          read(cmd_sock_cl, &close_after_send, 1);
          send_data_len = be32toh(send_data_len);

          printf("command: send data %u byte(s)", send_data_len);

          // send data
          d = send_data_len / DATA_BUF_SIZE;
          r = send_data_len % DATA_BUF_SIZE;
          for(i=0; i<d; i++) write(test_sock_cl, send_data_buf, DATA_BUF_SIZE);
          if(r>0) write(test_sock_cl, send_data_buf, r);

          // close socket if necessary
          if(close_after_send != 0)
          {
            if(close_after_send == 1)
            {
              printf(" and close()");
              close(test_sock_cl);
            }
            else if(close_after_send == 2)
            {
              printf(" and shutdown()");
              shutdown(test_sock_cl, SHUT_WR);
            }
            test_sock_cl = SOCK_FREE;
            test_conn_closed = 1;
          }
          printf("\n");
          break;

        case CMD_PORT:
          last_port_be = htons(*last_port);
          write(cmd_sock_cl, &last_port_be, 2);
          break;

        case CMD_IFCFG:
          printf("command: ip addr");
          read(cmd_sock_cl, &ifconfig_status, 1);
          if(ifconfig_status == 1)
          {
            printf(" add");
            system("/sbin/ip addr add dev eth0 10.2.1.3/24");
          }
          else
          {
            printf(" del");
            system("/sbin/ip addr del dev eth0 10.2.1.3/24");
          }
          printf("\n");
          break;

        case '\n':
          break;
        default:
          printf("unknown command\n");
          break;
        }
      }
      else {
        printf("command connection lost\n");
        munmap(last_port, sizeof(int));
        exit(EXIT_SUCCESS);
      }
    }

    if(test_conn_closed)
    {
      // increment test port number
      *last_port = (*last_port==TEST_PORT_END)? TEST_PORT_START: *last_port+1;

      listen_addr_test = (struct sockaddr_in) {
        .sin_family = AF_INET,
        .sin_addr.s_addr = INADDR_ANY,
        .sin_port = htons(*last_port)
      };

      close(test_sock_listen);

      if((test_sock_listen = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        perror("ERROR opening socket");

      if(bind(test_sock_listen, (struct sockaddr *) &listen_addr_test, sizeof(struct sockaddr_in)) < 0)
          perror("ERROR on binding");

      if(listen(test_sock_listen, 5) < 0)
        perror("ERROR on listen");

      printf("listening for test connection on port %i\n", *last_port);
    }

    /* rebuild select list */

    FD_ZERO(&rfds);
    FD_SET(cmd_sock_cl, &rfds);
    FD_SET(test_sock_listen, &rfds);
    nfds = max(test_sock_listen, cmd_sock_cl)+1;
    if(test_sock_cl != SOCK_FREE && !test_conn_remote_closed)
    {
      FD_SET(test_sock_cl, &rfds);
      nfds = max(nfds, test_sock_cl)+1;
    }
  }

  return 0;
}
