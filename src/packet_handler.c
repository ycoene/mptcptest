/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include <CUnit/CUnit.h>
#include <pthread.h>
#include <stddef.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "barrier.h"
#include "mptcp.h"
#include "packet_handler.h"
#include "tcp.h"
#include "testcases.h"
#include "server.h"

void dbg_print_hlist(struct hlist *hl)
{
  handler_t *h;
  printf("hlist=[");
  for(h = hl->lh_first; h != NULL; h = h->entries.le_next)
  {
    printf("(h=%p)->", h);
  }
  printf("NULL]\n");
}

void pre_handler(const struct pcap_pkthdr *header, const u_char *packet, subflow *in_sf)
{
  tcp_hdr_net *thn = get_tcp_hdr(packet);
  tcp_hdr_loc thl = tcp_hdr_dec(thn);
  f->rcv_wnd = thl.win;
  tcp_opt_ws *wscale;

  // update window size
  if((wscale=(tcp_opt_ws *)get_tcp_opt(TCP_OPT_WS, 0, thn))!=NULL)
  {
    f->rcv_wnd <<= wscale->shift_cnt;
  }

  if(thl.flags & TH_ACK)
  {
    in_sf->snd_una = thl.ack;
  }

  // received next in-order segment
  if(thl.seq == in_sf->rcv_nxt)
  {
    in_sf->rcv_nxt += payload_len(header, packet);

    if(thl.flags & TH_FIN)
      in_sf->rcv_nxt++;
  }

  if(c_recv_dack(NULL, header, packet, in_sf)==TEST_CONT)
  {
    dss_net *dssn = (dss_net *) get_tcp_opt(TCP_OPT_MPTCP, MPTCP_SUBTYPE_DSS, thn);
    dss_loc dssl = dss_dec(dssn);

    /* update flow state */
    // TODO sanity checks on DATA_ACK
    if(dssl.dack_len==8)
    {
      f->snd_una = dssl.dack;
    }
    else // dssl.dack_len==4
    {
      uint32_t diff = dssl.dack-(uint32_t)f->snd_una;
      f->snd_una += diff;
    }
  }
}

void default_handler(const struct pcap_pkthdr *header, const u_char *packet, subflow *in_sf)
{
  tcp_hdr_loc thl = tcp_hdr_dec(get_tcp_hdr(packet));

  /* Check for RST */
  if(thl.flags&TH_RST)
  {
    CU_FAIL("Received unexpected RST");
  }
}

void hlist_exec_matching(const struct pcap_pkthdr *header, const u_char *packet, subflow *in_sf, struct hlist *hl)
{
  handler_t *h = hl->lh_first, *h_next;

  int handler_found = 0, retval;

  while(h != NULL)
  {
    // lookup next handler before possibly releasing main thread.
    h_next = h->entries.le_next;

    if((h->sf == NULL || in_sf == h->sf) &&
        (retval=h->f(h->args, header, packet, in_sf)) != TEST_IRRELEVANT_PKT)
    {
      handler_found = 1;

#ifdef DEBUG
      printf("matching handler found for incoming packet c=%x: h=%p\n",
          get_tcp_hdr(packet)->checksum, h);
#endif

      if(h->reusable)
      {
        handler_match *hm = (handler_match *) malloc(sizeof(handler_match));
        bzero(hm, sizeof(handler_match));

        hm->in_sf = (subflow *) in_sf;
        hm->hdr = *header;
        hm->retval = retval;

        if(h->copy_packet)
        {
          hm->packet = (u_char *) malloc(header->caplen);
          memcpy(hm->packet, packet, header->caplen);
        }

        TAILQ_INSERT_TAIL(&h->match_queue_head, hm, entries);
      }
      else
      {
        LIST_REMOVE(h, entries);
        
        h->in_sf = (subflow *) in_sf;
        h->hdr = *header;
        h->retval = retval;

        if(h->copy_packet)
        {
          h->packet = (u_char *) malloc(header->caplen);
          memcpy(h->packet, packet, header->caplen);
        }
      }

      pthread_mutex_lock(&h->barrier.count_lock);
      h->barrier.match_count++;
      pthread_cond_broadcast(&h->barrier.ok_to_proceed);
      if(h->hs != NULL) // support of handler_select
      {
        pthread_mutex_lock(&h->hs->mutex);
        h->hs->count++;
        pthread_cond_broadcast(&h->hs->cond);
        pthread_mutex_unlock(&h->hs->mutex);
      }
      pthread_mutex_unlock(&h->barrier.count_lock);

    }
    h = h_next;
  }

  if(!handler_found)
  {
    default_handler(header, packet, in_sf);
#ifdef DEBUG
    printf("no matching handler found for incoming packet c=%x\n", get_tcp_hdr(packet)->checksum);
#endif
  }

#ifdef DEBUG
//      dbg_print_hlist(hl);
#endif
}

void hlist_exec_timeout(handler_t *h, struct hlist *hl)
{
  h->retval = h->f(NULL, NULL, NULL, NULL);
  h->packet = NULL;
  LIST_REMOVE(h, entries);
}

void mylib_init_barrier(barrier_t *b) {
  b->match_count = 0;
  pthread_mutex_init(&(b->count_lock), NULL);
  pthread_cond_init(&(b->ok_to_proceed), NULL);
}

int register_handler(handler_t *h)
{
  // (re)initialize output values
  h->retval = 0;
  h->packet = NULL;
  h->in_sf = NULL;

#ifdef DEBUG
  printf("register handler h=%p ", h);
#endif

  /* add new handler at the end of the handler list */
  memset(&h->entries, 0, sizeof(h->entries));
  LIST_INSERT_HEAD(&f->hl_head, h, entries);

  mylib_init_barrier(&h->barrier);
  if(h->reusable)
    TAILQ_INIT(&h->match_queue_head);

#ifdef DEBUG
//  dbg_print_hlist(&f->hl_head);
#endif

  return 0;
}

void pcap_callback(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{
  tcp_hdr_loc thl = tcp_hdr_dec(get_tcp_hdr(packet));
  subflow *sf = (subflow *)args;

#ifdef DEBUG
  uint16_t c = get_tcp_hdr(packet)->checksum;
  printf("received packet c=%x\n", c);
#endif

  /* incoming packets are processed one by one */
  SUSPEND_PACKET_TREATMENT();
  /* Received packet from unexpected port, probably from a previous test connection */
  if(sf->state == CLOSED || (
      (sf->rport != thl.sp || ((ip_hdr_net *)(packet+SIZE_ETHER))->ip_src != sf->raddr) &&
      !(sf->state == LISTEN && thl.flags == TH_SYN)))
  {
    RESUME_PACKET_TREATMENT();
    return;
    // TODO implement the same behavior by updating pcap filters
  }
  pre_handler(header, packet, (subflow *)args);
  hlist_exec_matching(header, packet, (subflow *)args, &f->hl_head);
  RESUME_PACKET_TREATMENT();

#ifdef DEBUG
  printf("flow state: snd_nxt=%lu snd_una=%lu rcv_nxt=%lu rcv_wnd=%u\n",
      f->snd_nxt, f->snd_una, f->rcv_nxt, f->rcv_wnd);
#endif
}

int send_recv(void (*send_f)(), handler_t *h)
{
  SUSPEND_PACKET_TREATMENT();
  register_handler(h);

  /* Execute action */
  send_f();
  RESUME_PACKET_TREATMENT();

  return recv_packet(h);
}

int recv_packet(handler_t *h)
{
  barrier_t *b = &h->barrier;

  /* Set expiration time */
  struct timespec tp;
  clock_gettime(CLOCK_REALTIME, &tp);
  tp.tv_sec += 1;
  int timedout = 0;

  pthread_mutex_lock(&b->count_lock);
  if(b->match_count == 0)
  {
    /* Block until response processed */
    if(pthread_cond_timedwait(&b->ok_to_proceed, &b->count_lock, &tp) == ETIMEDOUT)
    {
#ifdef DEBUG
      printf("handler %p timed out\n", h);
#endif
      timedout = 1;
    }
  }
  if(!timedout)
  {
    b->match_count--;
    if(h->hs != NULL) // support of handler_select
    {
      pthread_mutex_lock(&h->hs->mutex);
      h->hs->count--;
      pthread_cond_broadcast(&h->hs->cond);
      pthread_mutex_unlock(&h->hs->mutex);
    }
  }
  pthread_mutex_unlock(&b->count_lock);

  if(timedout)
  {
    SUSPEND_PACKET_TREATMENT();
    hlist_exec_timeout(h, &f->hl_head);
    RESUME_PACKET_TREATMENT();
  }
  else if(h->reusable)
  {
    SUSPEND_PACKET_TREATMENT();
    handler_match *hm = h->match_queue_head.tqh_first;
    h->hdr = hm->hdr;
    h->packet = hm->packet;
    h->in_sf = hm->in_sf;
    h->retval = hm->retval;
    TAILQ_REMOVE(&h->match_queue_head, hm, entries);
    free(hm);
    RESUME_PACKET_TREATMENT();
  }

  return h->retval;
}

int handler_is_ready(handler_t *h)
{
  return h->barrier.match_count;
}

int handler_reset(handler_t *h)
{
  // clear match list
  while(h->match_queue_head.tqh_first != NULL)
  {
    handler_match *tqh_first_old = h->match_queue_head.tqh_first;
    TAILQ_REMOVE(&h->match_queue_head, h->match_queue_head.tqh_first, entries);
    free(tqh_first_old->packet);
    free(tqh_first_old);
  }
//  free(h->packet);
  // reset output fields
  h->barrier = (barrier_t){};
  h->hdr = (struct pcap_pkthdr){};
  h->in_sf = NULL;
  h->packet = NULL;
  h->retval = 0;

  return 0;
}

int unregister_handler(handler_t *h)
{
  SUSPEND_PACKET_TREATMENT();
  handler_reset(h);
  LIST_REMOVE(h, entries);
  RESUME_PACKET_TREATMENT();

  return 0;
}

int handler_unregister_all()
{
  SUSPEND_PACKET_TREATMENT();
  while(f->hl_head.lh_first != NULL)
  {
    handler_reset(f->hl_head.lh_first);
    LIST_REMOVE(f->hl_head.lh_first, entries);
  }
  RESUME_PACKET_TREATMENT();

  return 0;
}

void handler_set(handler_t *h, struct handler_set *hs)
{
  pthread_mutex_lock(&h->barrier.count_lock);
  pthread_mutex_lock(&hs->mutex);
  h->hs = hs;
  hs->count += h->barrier.match_count;
  pthread_mutex_unlock(&hs->mutex);
  pthread_mutex_unlock(&h->barrier.count_lock);
  struct hln *hln = malloc(sizeof(struct hln));
  hln->h = h;
  h->hsl_member = hln;
  LIST_INSERT_HEAD(&h->hs->hl, hln, entries);
}

void handler_clr(handler_t *h, struct handler_set *hs)
{
  pthread_mutex_lock(&h->barrier.count_lock);
  pthread_mutex_lock(&hs->mutex);
  h->hs = NULL;
  hs->count -= h->barrier.match_count;
  pthread_mutex_unlock(&hs->mutex);
  pthread_mutex_unlock(&h->barrier.count_lock);
  LIST_REMOVE(h->hsl_member, entries);
  free(h->hsl_member);
}

void handler_set_init(struct handler_set *hs)
{
  hs->count = 0;
  LIST_INIT(&hs->hl);
  pthread_cond_init(&hs->cond, NULL);
  pthread_mutex_init(&hs->mutex, NULL);
}

void handler_set_destroy(struct handler_set *hs)
{
  pthread_cond_destroy(&hs->cond);
  pthread_mutex_destroy(&hs->mutex);
}

handler_t *handler_select(struct handler_set *hs, int nonblock)
{
  if(!nonblock)
  {
    pthread_mutex_lock(&hs->mutex);
    if(hs->count == 0)
    {
      pthread_cond_wait(&hs->cond, &hs->mutex);
    }
    pthread_mutex_unlock(&hs->mutex);
  }

  struct hln *hln;
  handler_t *h, *first = NULL;
  LIST_FOREACH(hln, &hs->hl, entries)
  {
    h = hln->h;
    if(handler_is_ready(h)) // transition taken
    {
      // consider most recently arrived packet
      if(first == NULL || timercmp(&h->match_queue_head.tqh_first->hdr.ts, &h->match_queue_head.tqh_first->hdr.ts, <))
        first = h;
    }
  }

  return first;
}
