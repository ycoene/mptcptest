/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "testcases.h"
#define indexof(array, elt) (((void *)elt-(void *)array)/sizeof(array[0]))

void b_new_sf_send_syn()
{
  subflow *sf = f->asf;
  sf->rport = remote_get_port();

  mp_join_loc mpjl = {
      .b = sf->ns_o.syn_b,
      .addrid = indexof(f->sf, sf), // TODO handle this properly
      .rtok = f->rtok,
      .snonce = sf->ns_o.lnonce
  };
//  *((int *)&mpjl.snonce) = libnet_get_prand(LIBNET_PRu32);

  if(sf->ns_o.syn_unknown_token) mpjl.rtok.data[0] += 1;

  mp_join_net mpjn = mp_join_enc(&mpjl, TH_SYN);
  tcp_hdr_loc hdr = tcp_hdr_auto(sf, TH_SYN);
  send_tcp_seg(sf, &hdr, (u_char *) &mpjn, mpjn.length, NULL, 0);

  sf->state = SYN_SENT;
}

int c_new_sf_recv_synack(C_ARGS)
{
  T_ASSERT_RET_ON_FAIL(packet!=NULL, "SYN/ACK exchange", TEST_STOP);

  tcp_hdr_net *thn = get_tcp_hdr(packet);

  if(in_sf->ns_o.syn_unknown_token)
  {
    T_ASSERT(thn->flags&&TH_RST, "SYN with unknown token");
    return TEST_STOP;
  }

  mp_join_net *ans_mpjn = (mp_join_net *) get_tcp_opt(TCP_OPT_MPTCP, MPTCP_SUBTYPE_MP_JOIN, thn);

  T_ASSERT_RET_ON_FAIL(ans_mpjn!=NULL, "MP_JOIN present", TEST_STOP);
  T_ASSERT(ans_mpjn->length == 16, "MP_JOIN format on SYN/ACK");
  T_ASSERT((ans_mpjn->subb & MP_JOIN_SYN_RSVD_MASK) == 0, "MP_JOIN SYN reserved bits");

  mp_join_loc ans_mpjl = mp_join_dec(ans_mpjn, TH_SYN|TH_ACK);

  uint8_t trhmac[SHA_DIGEST_LENGTH];
  in_sf->ns_o.rnonce = ans_mpjl.snonce;
  hmac_sha1(trhmac, &f->rkey, &f->lkey, &ans_mpjl.snonce, &in_sf->ns_o.lnonce);

  T_ASSERT(memcmp(trhmac, &ans_mpjl.sthmac, sizeof(key64))==0, "HMAC on SYN/ACK");

  tcp_hdr_loc thl = tcp_hdr_dec(thn);
  in_sf->rcv_nxt = thl.seq+1;

  return TEST_CONT;
}

int t_new_sf_send_syn_recv_synack()
{
  return send_recv(b_new_sf_send_syn, &(handler_t){.f=c_new_sf_recv_synack});
}

int c_nop(C_ARGS)
{
  return TEST_CONT;
}

void b_new_sf_send_ack()
{
  subflow *sf = f->asf;
  sf->snd_nxt += 1;

  mp_join_loc mpjl;
  hmac_sha1(mpjl.shmac, &f->lkey, &f->rkey, &sf->ns_o.lnonce, &sf->ns_o.rnonce);

  if(sf->ns_o.ack_unexpected_hmac)
    mpjl.shmac[0]+=1;

  tcp_hdr_loc hdr = tcp_hdr_auto(sf, TH_ACK);
  mp_join_net mpjn = mp_join_enc(&mpjl, TH_ACK);

  if(!sf->ns_o.ack_without_mp_join)
    send_tcp_seg(sf, &hdr, (u_char *)&mpjn, mpjn.length, NULL, 0);
  else
    send_tcp_seg(sf, &hdr, NULL, 0, NULL, 0);

  sf->state = ESTABLISHED;
}

int c_new_sf_recv_4thack(C_ARGS)
{
  subflow *sf = f->asf;

  T_ASSERT_RET_ON_FAIL(packet != NULL, "Fourth ACK", TEST_STOP);

  tcp_hdr_loc thl = tcp_hdr_dec(get_tcp_hdr(packet));

  if(sf->ns_o.ack_unexpected_hmac)
    T_ASSERT_RET_ON_PASS(thl.flags==TH_RST, "Unexpected HMAC on ACK", TEST_STOP);

  if(sf->ns_o.ack_without_mp_join)
    T_ASSERT_RET_ON_PASS(thl.flags==TH_RST, "ACK without MP_JOIN", TEST_STOP);

  T_ASSERT(thl.flags==TH_ACK, "Fourth ACK");

  return TEST_CONT;
}

int t_new_sf_send_ack_recv_4thack()
{
  return send_recv(b_new_sf_send_ack, &(handler_t){.f=c_new_sf_recv_4thack});
}

int new_sf_handshake(subflow *sf)
{
  f->asf = (sf==NULL)? f->asf: sf;
  int ret;

  T_CHECK((ret=send_recv(b_new_sf_send_syn,
      &(handler_t){.f = c_new_sf_recv_synack, .sf = f->asf})));
  T_CHECK((ret=send_recv(b_new_sf_send_ack,
      &(handler_t){.f=c_new_sf_recv_4thack, .sf = f->asf})));

  cleanup:
  return ret;
}

void ts_new_sf_ts_sut()
{
  test_case_init(2);

  int j, c;

  int syn_b[] = {0, 1};

  ARRAY_FOREACH_IDX(j, syn_b)
  for(c=0; c<4; c++)
  {
    bzero(&f->sf[1]->ns_o, sizeof(f->sf[1]->ns_o));
    f->sf[1]->ns_o.syn_b = syn_b[j];

    switch (c) { // the following events cannot occur together
      case 0:
        break;
      case 1:
        f->sf[1]->ns_o.ack_unexpected_hmac = 1;
        break;
      case 2:
        f->sf[1]->ns_o.syn_unknown_token = 1;
        break;
      case 3:
        f->sf[1]->ns_o.ack_without_mp_join = 1;
        break;
    }

    T_CHECK(init_sf_handshake(f->sf[0]));

    T_CHECK(new_sf_handshake(f->sf[1]));

    cleanup:
    b_flow_fastclose();
  }

  test_case_free();
}

void ts_mp_join_b()
{
  test_case_init(2);
  init_sf_handshake(f->sf[0]);
  f->sf[1]->ns_o.syn_b = 1;
  new_sf_handshake(f->sf[1]);
  handler_t dack = {H_CF(c_recv_dsn, CONC(.dsn = f->rcv_nxt))};
  register_handler(&dack);
  b_remote_send_data(1, 0);
  recv_packet(&dack);
  T_ASSERT(dack.in_sf != f->sf[1], "MP_JOIN SYN flags > Flag B");
  b_flow_fastclose();
  test_case_free();
}

int c_new_sf_recv_syn(C_ARGS)
{
  if(packet == NULL) return TEST_STOP;
  tcp_hdr_net *thn = get_tcp_hdr(packet);
  if(!(thn->flags & TH_SYN)) return TEST_IRRELEVANT_PKT;
  mp_join_net *mpjn = (mp_join_net *)
      get_tcp_opt(TCP_OPT_MPTCP, MPTCP_SUBTYPE_MP_JOIN, thn);
  if(mpjn == NULL) return TEST_IRRELEVANT_PKT;
  mp_join_loc mpjl = mp_join_dec(mpjn, TH_SYN);
  T_ASSERT(mpjn->length == 12, "MP_JOIN format on SYN");

  T_ASSERT_RET_ON_FAIL(memcmp(&mpjl.rtok, &f->ltok, sizeof(token32))==0,
      "Token on SYN", TEST_STOP);
  T_ASSERT((mpjn->subb & 0x0E) == 0, "Reserved flags");
  in_sf->ns_o.rnonce = mpjl.snonce;

  /* bind local subflow */
  tcp_hdr_loc thl = tcp_hdr_dec(thn);
  in_sf->rport = thl.sp;
  in_sf->raddr = ((ip_hdr_net *)(packet+SIZE_ETHER))->ip_src;
  in_sf->ptag_ip = libnet_build_ipv4(LIBNET_IPV4_H + LIBNET_TCP_H, 0, 0, 0, 64, IPPROTO_TCP, 0,
      in_sf->laddr, in_sf->raddr, NULL, 0, in_sf->l, in_sf->ptag_ip);
  in_sf->rcv_nxt = thl.seq+1;
  in_sf->state = SYN_RECEIVED;

  return TEST_CONT;
}

void b_new_sf_send_synack(subflow *sf)
{
  uint8_t hmac[SHA_DIGEST_LENGTH];
  hmac_sha1(hmac, &f->lkey, &f->rkey, &sf->ns_o.lnonce, &sf->ns_o.rnonce);

  mp_join_loc mpjl = {
      .sthmac = *(key64 *)hmac,
      .snonce = sf->ns_o.lnonce,
  };
  mp_join_net mpjn = mp_join_enc(&mpjl, TH_SYN|TH_ACK);

  tcp_hdr_loc hdr = tcp_hdr_auto(sf, TH_SYN|TH_ACK);
  send_tcp_seg(sf, &hdr, (u_char *)&mpjn, mpjn.length, NULL, 0);
}

int c_new_sf_recv_ack(C_ARGS)
{
  if(packet == NULL) return TEST_STOP;
  tcp_hdr_net *thn = get_tcp_hdr(packet);

  if(!T_ASSERT(!(thn->flags & TH_RST), "RST instead of ACK+MP_JOIN"))
    return TEST_STOP;

  mp_join_net *mpjn = (mp_join_net *) get_tcp_opt(TCP_OPT_MPTCP,
      MPTCP_SUBTYPE_MP_JOIN, thn);
  if(!T_ASSERT(mpjn != NULL, "MP_JOIN present")) return TEST_STOP;

  u_char hmac[SHA_DIGEST_LENGTH];
  hmac_sha1(hmac, &f->rkey, &f->lkey, &in_sf->ns_o.rnonce, &in_sf->ns_o.lnonce);
  if(!T_ASSERT(memcmp(hmac, mpjn->shmac, SHA_DIGEST_LENGTH) == 0,
      "HMAC on ACK")) return TEST_STOP;

  return TEST_CONT;
}

int c_recv_data(C_ARGS)
{
  if(packet != NULL && payload_len(hdr, packet)>0) return TEST_CONT;
  else return TEST_IRRELEVANT_PKT;
}

void ts_new_sf_sutts()
{
  f = flow_init();
  f->nsf = 2;
  f->sf[0] = subflow_init(cfg.loc_addr[0]);
  f->sf[1] = subflow_init(cfg.loc_addr[0]);
  f->asf = f->sf[0];

  *(uint32_t *) &f->sf[1]->ns_o.lnonce = libnet_get_prand(LIBNET_PRu32);

  int *d, data[] = {0, 1};
//  int *mm, missing_synack_mpj = {0, 1};
//  int *uh, unexp_synack_hmac = {0, 1};

  ARRAY_FOREACH(d, data)
  {
    T_CHECK(init_sf_handshake_sutts(f->sf[0]));

    handler_t mpj_syn = {.f = c_new_sf_recv_syn, .sf = f->sf[1]};
    handler_t mpj_ack = {.f = c_new_sf_recv_ack, .sf = f->sf[1]};
    handler_t data = {.f = c_recv_data, .sf = f->sf[1]};

    register_handler(&mpj_syn);
    f->sf[1]->state = LISTEN;
    b_remote_ifconfig(1);

    if(*d)
    {
      /*
       * Remote send two initial windows of data. Likely to trigger the
       * creation of a new subflow according to RFC 6824 §3.8.2.
       * Received data left unacknowledged.
       */
      b_remote_send_data(2*f->sf[0]->rcv_wnd, 0);
    }

    recv_packet(&mpj_syn);

    switch(*d) {
    /*
     * No data exchanged: we should *not* have received a SYN+MP_JOIN.
     */
    case 0:
      T_ASSERT(mpj_syn.in_sf == NULL, "New subflow before DSS");
      break;
    /*
     * Data exchanged: we should have received a SYN+MP_JOIN. If so, the
     * handshake can continue.
     */
    case 1:
      T_CHECK(mpj_syn.retval);
      register_handler(&mpj_ack);
      register_handler(&data);
      b_new_sf_send_synack(f->sf[1]);
      T_CHECK(recv_packet(&mpj_ack));

      /* Try to trigger data transfer during PRE_ESTABLISHED state */
      b_remote_send_data(2*f->sf[0]->rcv_wnd, 0);

      /* Wait for retransmission of the (not acknowledged) ACK */
      register_handler(&mpj_ack);
      recv_packet(&mpj_ack);
      T_ASSERT(mpj_ack.in_sf != NULL, "Missing fourth ACK");

      T_ASSERT(data.in_sf == NULL, "PRE_ESTABLISHED state");

      break;
    }

    cleanup:
    b_remote_ifconfig(0);
    usleep(100e3);
    b_flow_fastclose();
  }

  test_case_free();
}
