/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include <iniparser.h>

#define MAX_HOST_ADDR 16

struct config {
  dictionary *d;
  char *loc_addr[MAX_HOST_ADDR];
  char *rem_addr[MAX_HOST_ADDR];
} cfg;

int load_config();

#endif /* CONFIG_H_ */
