/**
 * @file      callback_args.h
 * @brief     Facilities for manipulating functions with indirect parameters
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 *
 *  Such functions are in the form <tt>type f(void *args, ...);</tt>
 */

#ifndef PTR_ARGS_H_
#define PTR_ARGS_H_

/**
 * Concatenate comma-separated arguments.
 * Used to pass an argument list to a macro as one argument.
 */
#define CONC(...) __VA_ARGS__

/**
 * Argument structure type for function \c fname.
 *
 * e.g. \c CF_ARG_TYPE(f) expands as <tt>struct f_arg</tt>
 */
#define CF_ARG_TYPE(fname) struct fname ## _arg

/**
 * Declare a function with indirect parameters along with its associated
 * parameter structure.
 *
 * e.g. <tt>CF_DEF(int, f, void *a, {int b;});</tt> expands as
 * <tt>struct f_arg {int b;}; int  f(void *a);</tt>
 */
#define CF_DEF(rettype, fname, args, sargs)         \
  CF_ARG_TYPE(fname) sargs;                         \
  rettype fname(args)

/**
 * Call a function with indirect parameters.
 */
#define CF_CALL(fname, args, sargs) ({              \
    CF_ARG_TYPE(fname) fname ## _arg = {sargs};     \
    fname(&fname ## _arg, args);                    \
  })

/**
 * Declare pointer \c arg as \c void pointer \c args casted to function
 * \c fname parameter structure type. Use within a function with indirect
 * parameters.
 */
#define CF_GET_ARG(fname) CF_ARG_TYPE(fname) *arg = (CF_ARG_TYPE(fname)*) args;

/**
 * Automatically fill \c .f and \c .args members for function \c fname in a
 * designated initializer.
 *
 * Used when declaring handlers.
 */
#define H_CF(fname, fargs) .f = fname, .args = &(CF_ARG_TYPE(fname)){fargs}

#define CFA(base, fname, fargs) .base = fname, .base ## _args = &(CF_ARG_TYPE(fname)){fargs}

#endif /* PTR_ARGS_H_ */
