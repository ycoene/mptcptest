/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include <endian.h>
#include <pthread.h>
#include <stdlib.h>
#include "mptcp.h"
#include "packet_handler.h"
#include "server.h"

#define SIZE_ETHER 14 /* Ethernet header size in bytes */
#define SIZE_TCP_HDR_BASE 20 /* TCP header size without options in bytes */

flow *flow_init() {
  flow *f = (flow *)malloc(sizeof(flow));
  bzero(f, sizeof(flow));
  LIST_INIT(&f->hl_head);

  /* generate local key */
//  f->lkey = *(key64 *)((uint8_t[8]){0, 0, 0, 0, 0, 0, 0, 1});
  *(uint32_t *)(&f->lkey.data[0]) = libnet_get_prand(LIBNET_PRu32);
  *(uint32_t *)(&f->lkey.data[4]) = libnet_get_prand(LIBNET_PRu32);

  uint8_t hash[SHA_DIGEST_LENGTH];
  hash_key_sha1(hash, f->lkey);
  f->ltok = *(token32*)hash;
  return f;
}

void *pcap_loop_wrapper(void *sf) {
  pcap_loop(((subflow *)sf)->p, -1, pcap_callback, (u_char *) sf);
  pthread_exit(NULL);
  return NULL;
}

void subflow_free(subflow *sf)
{
  void *res;
  pcap_breakloop(sf->p);

  // destroy subflow thread
  if(pthread_cancel(sf->t) != 0)
    perror("pthread_cancel");
  if(pthread_join(sf->t, &res) != 0)
    perror("pthread_join");

//  pcap_close(sf->p);
  libnet_destroy(sf->l);
  free(sf);
}

subflow *subflow_init(char *devname) {
  subflow *sf = (subflow *)malloc(sizeof(subflow));
  bzero(sf, sizeof(subflow));

  /* default values */
  sf->state = CLOSED;
  sf->lport = PORT_SUT;
  sf->rport = port_tst;
  sf->raddr = libnet_name2addr4(sf->l, cfg.rem_addr[0], LIBNET_RESOLVE);
  sf->rcv_wnd = 512;
  libnet_seed_prand(sf->l);
  sf->snd_nxt = libnet_get_prand(LIBNET_PR32); // random initial sequence number
  sf->ci_o.mp_cap_version = MPTCP_VERSION;
  sf->ci_o.mp_cap_flags_ts = MP_CAP_A|MP_CAP_H;
  
  /* create libnet context */
  if((sf->l = libnet_init(LIBNET_RAW4, devname, errbuf)) == NULL) {
    fprintf(stderr, "libnet_init() failed: %s\n", errbuf);
    exit(EXIT_FAILURE);
  }
  
  sf->laddr = libnet_get_ipaddr4(sf->l);

  /* build TCP options */
  if((sf->ptag_tcp_opt = libnet_build_tcp_options(0, 0, sf->l, 0)) == -1) {
    fprintf(stderr, "Error building TCP header: %s\n", libnet_geterror(sf->l));
    libnet_destroy(sf->l);
    exit(EXIT_FAILURE);
  }
  
  /* build TCP header */
  if((sf->ptag_tcp = libnet_build_tcp(
      sf->lport, sf->rport, sf->snd_nxt, 0, 0, sf->rcv_wnd, 0, 0, 0, 0, 0, sf->l, 0)) == -1) {
    fprintf(stderr, "Error building TCP header: %s\n", libnet_geterror(sf->l));
    libnet_destroy(sf->l);
    exit(EXIT_FAILURE);
  }

  /* build IPv4 header */
  if((sf->ptag_ip = libnet_autobuild_ipv4(LIBNET_IPV4_H + LIBNET_TCP_H, IPPROTO_TCP, sf->raddr, sf->l)) == -1) {
    fprintf(stderr, "Error building IPv4 header: %s\n", libnet_geterror(sf->l));
    libnet_destroy(sf->l);
    exit(EXIT_FAILURE);
  };

  struct bpf_program fp;
  char filter_exp[128];
  sprintf(filter_exp, "dst host %s and dst port %d", libnet_addr2name4(sf->laddr, LIBNET_DONT_RESOLVE), sf->lport);
  bpf_u_int32 mask;
  bpf_u_int32 net;

  /* Find the properties for the device */
  if(pcap_lookupnet(devname, &net, &mask, errbuf) == -1) {
    fprintf(stderr, "Couldn't get netmask for device %s: %s\n", devname, errbuf);
    net = 0;
    mask = 0;
  }

  /* create pcap capture handle */
  sf->p = pcap_open_live(devname, BUFSIZ, 0, 1000, errbuf);
  if(sf->p == NULL) {
    fprintf(stderr, "Couldn't open device %s: %s\n", devname, errbuf);
    exit(EXIT_FAILURE);
  }

  /* Compile and apply the filter */
  if(pcap_compile(sf->p, &fp, filter_exp, 0, net) == -1) {
    fprintf(stderr, "Couldn't parse filter %s: %s\n", filter_exp,
        pcap_geterr(sf->p));
    exit(EXIT_FAILURE);
  }
  if(pcap_setfilter(sf->p, &fp) == -1) {
    fprintf(stderr, "Couldn't install filter %s: %s\n", filter_exp,
        pcap_geterr(sf->p));
    exit(EXIT_FAILURE);
  }

  pthread_create(&sf->t, NULL, &pcap_loop_wrapper, (void *)sf);

  return sf;
}

mp_cap_net mp_cap_enc(mp_cap_loc const *mpcl, uint tcp_flags) {
  mp_cap_net mpcn;
  mpcn.type = TCP_OPT_MPTCP;
  if(tcp_flags & TH_SYN)
    mpcn.length = 12;
  else { /* TH_ACK */
    mpcn.length = 20;
    mpcn.rkey = mpcl->rkey;
  }
  mpcn.subver = (mpcl->subtype<<4)+mpcl->version;
  mpcn.flags = mpcl->flags;
  mpcn.skey = mpcl->skey;
  return mpcn;
}

mp_cap_loc mp_cap_dec(mp_cap_net const *mpcn) {
  mp_cap_loc mpcl;
  mpcl.subtype = (mpcn->subver)>>4;
  mpcl.version = (mpcn->subver)&0x0F;
  mpcl.flags = mpcn->flags;
  mpcl.skey = mpcn->skey;
  mpcl.rkey = mpcn->rkey;
  return mpcl;
}

mp_join_net mp_join_enc(mp_join_loc const *o, uint tcp_flags) {

  mp_join_net mpjn = {
      .type = TCP_OPT_MPTCP
  };

  switch (tcp_flags) {
    case TH_SYN:
      mpjn.length = 12;
      mpjn.subb = (MPTCP_SUBTYPE_MP_JOIN << 4) + o->b;
      mpjn.addrid = o->addrid;
      mpjn.rtok = o->rtok;
      mpjn.snonce_syn = o->snonce;
      break;
    case (TH_SYN|TH_ACK):
      mpjn.length = 16;
      mpjn.subb = (MPTCP_SUBTYPE_MP_JOIN << 4) + o->b;
      mpjn.addrid = o->addrid;
      mpjn.sthmac = o->sthmac;
      mpjn.snonce_synack = o->snonce;
      break;
    case TH_ACK:
      mpjn.length = 24;
      mpjn.subb = (MPTCP_SUBTYPE_MP_JOIN << 4);
      memcpy(&mpjn.shmac, o->shmac, 20);
      break;
    default:
      break;
  }
  return mpjn;
}

mp_join_loc mp_join_dec(mp_join_net const *o, uint tcp_flags) {
  mp_join_loc mpjl;
  mpjl.b = o->subb&1;
  mpjl.addrid = o->addrid;
  switch (tcp_flags) {
    case TH_SYN:
      mpjl.rtok = o->rtok;
      mpjl.snonce = o->snonce_syn;
      break;
    case TH_SYN|TH_ACK:
      mpjl.sthmac = o->sthmac;
      mpjl.snonce = o->snonce_synack;
      break;
    case TH_ACK:
      memcpy(mpjl.shmac, &o->shmac, sizeof(mpjl.shmac));
      break;
    default:
      break;
  }
  return mpjl;
}

dss_net dss_enc(dss_loc *dssl) {
  int dack_len = dssl->dack_len, dsn_len = dssl->dsn_len;

  /* Compute DSS flags from field lengths (0, 4 or 8) */
  uint flags = dssl->flags;

  if(dssl->dack_len > 0) {
    flags |= DSS_A;

    if(dssl->dack_len == 8)
      flags |= DSS_a;
  }

  if(dssl->dsn_len > 0) {
    flags |= DSS_M;

    if(dssl->dsn_len == 8)
      flags |= DSS_m;
  }

  dss_net dssn = {
      .header.res_flags = flags,
      .header.type = TCP_OPT_MPTCP,
      .header.length = sizeof(dss_net_header)+dack_len+dsn_len+(dsn_len!=0?sizeof(dss_net_footer):0),
      .header.sub_res = MPTCP_SUBTYPE_DSS << 4
  };

  uint8_t *ptr = (uint8_t *) (&dssn.footer);

  if(dack_len == 4)
    *((uint32_t *)ptr) = htobe32(dssl->dack);
  else if(dack_len == 8)
    *((uint64_t *)ptr) = htobe64(dssl->dack);

  ptr = ptr+dack_len;

  if(dsn_len == 4)
    *((uint32_t *)ptr) = htobe32(dssl->dsn);
  else if(dsn_len == 8)
    *((uint64_t *)ptr) = htobe64(dssl->dsn);

  ptr = ptr+dsn_len;

  if(dsn_len != 0) {
    *((dss_net_footer *)ptr) = (dss_net_footer) {
      .ssn = htobe32(dssl->ssn),
      .dll = htobe16(dssl->dll),
      .checksum[0] = dssl->checksum[0],
      .checksum[1] = dssl->checksum[1]
    };
  }

  return dssn;
}

dss_loc dss_dec(dss_net *dssn) {
  int dack_len, dsn_len;

  if(dssn->header.res_flags & DSS_A)
    if(dssn->header.res_flags & DSS_a)
      dack_len = 8;
    else
      dack_len = 4;
  else
    dack_len = 0;

  if(dssn->header.res_flags & DSS_M)
    if(dssn->header.res_flags & DSS_m)
      dsn_len = 8;
    else
      dsn_len = 4;
  else
    dsn_len = 0;

  dss_loc dssl = {
    .subtype = (dssn->header.sub_res>>4)&0x0F,
    .flags = dssn->header.res_flags,
    .dack_len = dack_len,
    .dsn_len = dsn_len
  };

  if(dack_len == 4)
    dssl.dack = dsn_expand(f->snd_nxt, be32toh(*((uint32_t *)dssn->footer)));
  else if(dack_len == 8)
    dssl.dack = be64toh(*((uint64_t *)dssn->footer));

  if(dsn_len == 4)
    dssl.dsn = dsn_expand(f->rcv_nxt, be32toh(*((uint32_t *)&dssn->footer[dack_len])));
  else if(dsn_len == 8)
    dssl.dsn = be64toh(*((uint64_t *)&dssn->footer[dack_len]));

  if(dsn_len != 0) {
    dss_net_footer *dnf = (dss_net_footer *) &dssn->footer[dack_len+dsn_len];
    dssl.ssn = be32toh(dnf->ssn);
    dssl.dll = be16toh(dnf->dll);
    dssl.checksum[0] = dnf->checksum[0];
    dssl.checksum[1] = dnf->checksum[1];
  }

  return dssl;
}

// http://stackoverflow.com/questions/8845178/c-programming-tcp-checksum
// http://www.roman10.net/how-to-calculate-iptcpudp-checksumpart-1-theory/
void dss_do_checksum(dss_loc *dssl, uint8_t *dsn_map_data)
{
  int dll = dssl->dll;
  if(dssl->flags&DSS_F) --dll; // FIN not taken into account for checksum

  dss_pseudo_header_net dphn = {
      .dsn = htobe64(dssl->dsn),
      .ssn = htobe32(dssl->ssn),
      .data_len = htobe16(dssl->dll)
  };

  uint8_t *dphn_ptr = (uint8_t *)&dphn;
  int dphn_len = sizeof(dss_pseudo_header_net);

  uint32_t csum=0;

  while(dphn_len>1) // pseudo-header checksum
  {
    csum += be16toh(*((uint16_t *)dphn_ptr));
    dphn_len -= 2;
    dphn_ptr += 2;
  }
  while(dll>1) // mapped data checksum
  {
    csum += be16toh(*((uint16_t *)dsn_map_data));
    dll -= 2;
    dsn_map_data += 2;
  }
  if(dll==1) // odd length data
    csum += *((uint8_t *)dsn_map_data)<<8;

  csum = (csum>>16) + (csum&0xFFFF);
  csum += (csum>>16);
  csum = ~csum;

  *(uint16_t*)&dssl->checksum = htobe16((uint16_t)csum);
}

dss_loc dss_autofill(int dack_len, int dsn_len, uint8_t *dsn_map_data, int dll, uint32_t ssn, int dfin_set)
{
  dss_loc dssl = {
    .dack_len = dack_len,
    .dack = f->rcv_nxt,
    .dsn_len = dsn_len,
    .dsn = f->snd_nxt,
    .dll = dll,
    .ssn = ssn,
    .flags = (dfin_set)? DSS_F: 0
  };

  dss_do_checksum(&dssl, dsn_map_data);

  return dssl;
}

mp_fastclose_net mp_fastclose_enc(key64 key) {
  mp_fastclose_net mpfcn = {
      .kind = TCP_OPT_MPTCP,
      .length = sizeof(mp_fastclose_net),
      .subres = MPTCP_SUBTYPE_MP_FASTCLOSE<<4,
      .res = 0,
      .rkey = key
  };
  return mpfcn;
}

uint64_t dsn_expand(uint64_t ref, uint32_t new_lsb)
{
  uint64_t ret;
  uint32_t ref_lsb = (uint32_t)ref;

  uint32_t diff1 = new_lsb - ref_lsb;
  uint32_t diff2 = ref_lsb - new_lsb;

  if(diff1 <= diff2)
    ret = ref + diff1;
  else
    ret = ref - diff2;

  return ret;
}
