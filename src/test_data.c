/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "packet_handler.h"
#include "testcases.h"

void ts_ack_64bit_dsn()
{
  test_case_init(1);
  init_sf_handshake(f->sf[0]);
  u_char data[] = {'*'};
  dss_loc dss = dss_autofill(8, 8, data, sizeof(data), 1, 0);
  handler_t dack = {
      .f = c_recv_dack,
      .args = &(struct c_recv_dack_arg){.dack = f->snd_nxt+1},
      .copy_packet = 1
  };
  register_handler(&dack);
  b_send_data(f->sf[0], &dss, data, sizeof(data), TH_ACK);
  recv_packet(&dack);
  dss_loc ans_dss = dss_dec((dss_net *)get_tcp_opt(TCP_OPT_MPTCP, MPTCP_SUBTYPE_DSS, get_tcp_hdr(dack.packet)));
  T_ASSERT(ans_dss.dack_len==8, "ACK of 64-bit DSN");
  b_flow_fastclose();
  test_case_free();
  free(dack.packet);
}

void ts_data_ts_sut()
{
  test_case_init(2);

  T_CHECK(init_sf_handshake(NULL));

  f->asf = f->sf[1];
  T_CHECK(new_sf_handshake(NULL));
  f->asf = f->sf[0];

  // test for different dll values

  /* start data exchange */
  u_char *data = (u_char *) malloc(65536);
  int b;
  for(b=0; b<65536; b++) data[b] = b%0x100;
  int pl_len = 1e3;
  int map_len = 5e3;
  int map_count = 1;

  handler_t dack_h = {.f=c_recv_dack, .reusable=1};

  SUSPEND_PACKET_TREATMENT();
  register_handler(&dack_h);
  RESUME_PACKET_TREATMENT();

  int i;
  for(i=0; i<map_count; i++)
  {
    // segment with DSS
    dss_loc dssl = {
      .dack_len = 4,
      .dsn_len = 8,
      .dack = f->rcv_nxt,
      .dsn = f->snd_nxt,
      .dll = map_len,
      .ssn=1
    };

    dss_do_checksum(&dssl, data);

    b_send_data(NULL, &dssl, data+i*map_len, pl_len, TH_ACK);

    int offset;
    for(offset=pl_len; offset<map_len; offset+=pl_len)
    {
      usleep(pl_len/20e6*1e6); // reduce troughput
      b_send_data(NULL, NULL, data+offset, pl_len, TH_ACK);
    }
  }

  cleanup:
  b_flow_fastclose();
  test_case_free();
}

void ts_data_sut_ts()
{
  test_case_init(1);

  T_CHECK(init_sf_handshake(NULL));

  handler_t first_dss_h = {
      .f=c_recv_dss,
      .args=&(struct c_recv_dss_arg){.dack_len=DACKANY, .dsn_len=DSNANY},
      .copy_packet=1
  };
  register_handler(&first_dss_h);
  b_remote_send_data(1, 0);
  T_CHECK(recv_packet(&first_dss_h));

  dss_loc dssl = dss_dec((dss_net*)get_tcp_opt(TCP_OPT_MPTCP, MPTCP_SUBTYPE_DSS, get_tcp_hdr(first_dss_h.packet)));
  T_ASSERT(dssl.dsn==f->rcv_nxt, "Initial Data Sequence Number");

  cleanup:
  b_flow_fastclose();
  free(first_dss_h.packet);
  test_case_free();
}

void update_flow_state(C_ARGS) {
  tcp_hdr_net *thn = get_tcp_hdr(packet);
  dss_net *dssn = (dss_net *) get_tcp_opt(TCP_OPT_MPTCP, MPTCP_SUBTYPE_DSS, thn);
  if(dssn!=NULL) {
    /* DSS present: register new mapping */
    dss_loc dssl = dss_dec(dssn);
    if(dssl.flags & DSS_F) f->rcv_nxt += 1;
  }
  else {

  }
}

void b_send_data(subflow *sf, dss_loc *dssl, u_char *pl, int pl_len, uint flags)
{
  sf = (sf==NULL)? f->asf: sf;

  tcp_hdr_loc hdr = tcp_hdr_auto(sf, flags);

  if(dssl != NULL)
  {
    dss_net dssn = dss_enc(dssl);
    send_tcp_seg(sf, &hdr, (u_char *)&dssn, dssn.header.length, pl, pl_len);
  }
  else
  {
    send_tcp_seg(sf, &hdr, NULL, 0, pl, pl_len);
  }

  /* Flow/subflow state updates */
  sf->snd_nxt += pl_len;
  f->snd_nxt += pl_len;

  // DATA_FIN occupies 1 byte in data sequence space
  if(dssl != NULL && (dssl->flags & DSS_F))
    f->snd_nxt += 1;

  // FIN occupies 1 byte in subflow sequence space
  if(flags & TH_FIN)
    sf->snd_nxt += 1;
}

int c_recv_dss(C_ARGS)
{
  CF_GET_ARG(c_recv_dss);

  if(packet==NULL) return TEST_STOP;

  dss_net *dssn = (dss_net *) get_tcp_opt(TCP_OPT_MPTCP, MPTCP_SUBTYPE_DSS, get_tcp_hdr(packet));

  if(dssn==NULL)
    return TEST_IRRELEVANT_PKT;

  dss_loc dssl = dss_dec(dssn);

  if(   (arg->dack_len == DACK0 && dssl.dack_len != 0) ||
        (arg->dack_len == DACK4 && dssl.dack_len != 4) ||
        (arg->dack_len == DACK8 && dssl.dack_len != 8) ||
        (arg->dsn_len == DSN0 && dssl.dsn_len != 0) ||
        (arg->dsn_len == DSN4 && dssl.dsn_len != 4) ||
        (arg->dsn_len == DSN8 && dssl.dsn_len != 8) ||
        (arg->dack_len == DACKSET && dssl.dack_len == 0) ||
        (arg->dsn_len == DSNSET && dssl.dsn_len == 0)
        )
        return TEST_IRRELEVANT_PKT;

  return TEST_CONT;
}

int c_recv_dack(C_ARGS)
{
  if(packet==NULL) return TEST_STOP;

  dss_net *dssn = (dss_net *) get_tcp_opt(TCP_OPT_MPTCP, MPTCP_SUBTYPE_DSS, get_tcp_hdr(packet));

  if(dssn==NULL)
    return TEST_IRRELEVANT_PKT;

  dss_loc dssl = dss_dec(dssn);

  if(dssl.dack_len == 0)
    return TEST_IRRELEVANT_PKT;

  if(args!=NULL)
  {
    CF_GET_ARG(c_recv_dack);
    if(dssl.dack_len == 4)
    {
      if((uint32_t)arg->dack!=(uint32_t)dssl.dack)
        return TEST_IRRELEVANT_PKT;
    }
    else
      if(arg->dack!=dssl.dack)
        return TEST_IRRELEVANT_PKT;
  }

  return TEST_CONT;
}

int c_recv_dsn(C_ARGS)
{
  int ret;
  if((ret = CF_CALL(c_recv_dss, CONC(hdr, packet, in_sf),
      CONC(.dack_len = DACKANY, .dsn_len = DSNSET))) == TEST_CONT)
  {
    CF_GET_ARG(c_recv_dsn);
    dss_loc dssl = dss_dec((dss_net*)get_tcp_opt(TCP_OPT_MPTCP, MPTCP_SUBTYPE_DSS, get_tcp_hdr(packet)));
    if(arg->dsn != dssl.dsn)
      return TEST_IRRELEVANT_PKT;
    if(arg->dll != 0 && arg->dll != dssl.dll)
      return TEST_IRRELEVANT_PKT;
  }
  return ret;
}

int c_recv_ack(C_ARGS)
{
  if(packet==NULL)
    return TEST_STOP;

  tcp_hdr_loc thl = tcp_hdr_dec(get_tcp_hdr(packet));
  if(!(thl.flags&TH_ACK))
    return TEST_IRRELEVANT_PKT;

  if(args != NULL)
  {
    CF_GET_ARG(c_recv_ack);
    if(thl.ack != arg->ack)
      return TEST_IRRELEVANT_PKT;
  }

  return TEST_CONT;
}

void ts_dss_fuzz()
{
  test_case_init(1);

  u_char *pl = malloc(32768);

  int *pl_len, pl_len_array[] = {0, 1, 512};
  int *map_len, map_len_array[] = {0, 1, 512};
  int *tcp_flags, tcp_flags_array[] = {TH_ACK, TH_FIN, TH_SYN, TH_FIN|TH_ACK,
      TH_SYN|TH_ACK};
  uint *ssn, ssn_array[] = {1, 0, 0xFFFFFFFF};
  int *dfin_set, dfin_set_array[] = {0, 1};
  int *dsn, dsn_array[] = {4, 8, 0};
  int *dack, dack_array[] = {4, 8, 0};

  ARRAY_FOREACH(pl_len, pl_len_array)
  ARRAY_FOREACH(map_len, map_len_array)
  ARRAY_FOREACH(tcp_flags, tcp_flags_array)
  ARRAY_FOREACH(dfin_set, dfin_set_array)
  ARRAY_FOREACH(dsn, dsn_array)
  ARRAY_FOREACH(dack, dack_array)
  ARRAY_FOREACH(ssn, ssn_array)
  {
    init_sf_handshake(f->sf[0]);
    dss_loc dssl = dss_autofill(*dack, *dsn, pl, *map_len, *ssn, *dfin_set);
    b_send_data(f->sf[0], &dssl, (*pl_len==0)? NULL: pl, *pl_len, *tcp_flags);
    usleep(20e3);
    b_flow_fastclose();
  }
  test_case_free();
}
