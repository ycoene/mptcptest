/**
 * @file      server.h
 * @brief     Master-slave communication protocol parameters
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#ifndef SERVER_H_
#define SERVER_H_

#define CMD_CLOSE   0x30
#define CMD_INIT    0x31
#define CMD_SEND    0x32    //!< followed by: length(4B) | close(1B)
#define CMD_PORT    0x33    //!< answer: port(2B)
#define CMD_ADDR    0x34
#define CMD_IFCFG   0x35    //!< followed by: status(1B)

#define PORT_CMD    6824    //!< Command port the slave listens to
int     port_tst;           //!< Test port the slave is listening to
#define PORT_SUT    6826    //!< Test port the master listens to

int cmd_sock; /*< command socket */

#endif /* SERVER_H_ */
