/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "packet_handler.h"
#include "testcases.h"
#include "mptcp.h"

void b_init_sf_send_syn()
{
  subflow *sf = f->asf;
  sf->rport = remote_get_port();
  sf->isn = sf->snd_nxt;

  /* compute flow token and initial data sequence number *///TODO: move to subflow init
  uint8_t hash[SHA_DIGEST_LENGTH];
  hash_key_sha1(hash, f->lkey);

  f->ltok = *(token32 *)hash; // hash most significant 32 bits
  f->snd_nxt = *(uint64_t *)(hash+SHA_DIGEST_LENGTH-sizeof(uint64_t)); // hash least significant 64 bits
  f->snd_nxt = be64toh(f->snd_nxt);
  f->snd_una = f->snd_nxt;

  /* mp_capable option settings */
  mp_cap_loc mpcl = {
      .subtype = MPTCP_SUBTYPE_MP_CAPABLE,
      .version = sf->ci_o.mp_cap_version,
      .flags = sf->ci_o.mp_cap_flags_ts,
      .skey = f->lkey,
      .rkey = f->rkey
  };
  /* encode mp_capable option */
  mp_cap_net mpcn = mp_cap_enc(&mpcl, TH_SYN);

  /* send segment */
  tcp_hdr_loc hdr = tcp_hdr_auto(sf, TH_SYN);

  if(!sf->ci_o.syn_without_mp_cap)
    send_tcp_seg(sf, &hdr, (u_char *)&mpcn, mpcn.length, NULL, 0);
  else
    send_tcp_seg(sf, &hdr, NULL, 0, NULL, 0);

  f->snd_nxt++;
  sf->snd_nxt++;
  sf->state = SYN_SENT;
}

int c_init_sf_recv_synack(C_ARGS)
{
  if(f->asf->ci_o.mp_cap_flags_ts & MP_CAP_B)
  {
    T_ASSERT_RET_ON_FAIL(packet==NULL, "Flag B ignored", TEST_STOP);
    return TEST_CONT;
  }
  else
    T_ASSERT_RET_ON_FAIL(packet!=NULL, "send SYN/ACK upon SYN reception", TEST_STOP);

  tcp_hdr_net *thn = get_tcp_hdr(packet);
  if(thn->flags!=(TH_SYN|TH_ACK))
    return TEST_IRRELEVANT_PKT;

  mp_cap_net *ans_mpcn = (mp_cap_net *)get_tcp_opt(TCP_OPT_MPTCP, MPTCP_SUBTYPE_MP_CAPABLE, thn);
  if(in_sf->ci_o.syn_without_mp_cap || in_sf->ci_o.mp_cap_version != MPTCP_VERSION) {
    T_ASSERT(ans_mpcn==NULL, "Regular SYN");
    return TEST_STOP;
  }

  if((in_sf->ci_o.mp_cap_flags_ts&0x3F)==0) {
    T_ASSERT(ans_mpcn==NULL, "Flags C through H");
    return TEST_STOP;
  }
  else
    T_ASSERT_RET_ON_FAIL(ans_mpcn!=NULL, "SYN/ACK contains key", TEST_STOP);

  mp_cap_loc ans_mpcl = mp_cap_dec(ans_mpcn);

  T_ASSERT(is_set(MP_CAP_A, ans_mpcl.flags), "Flag A set to 1");
  T_ASSERT(!is_set(MP_CAP_B, ans_mpcl.flags), "Flag B set to 0");
  T_ASSERT((ans_mpcl.flags&0x3F)==1, "Flags C through H");
  T_ASSERT(ans_mpcl.version==0, "MPTCP version");
  T_ASSERT(memcmp(&ans_mpcl.skey, &ans_mpcl.rkey, sizeof(key64))!=0, "Key generation: repeated key");
  in_sf->ci_o.mp_cap_flags_sut = ans_mpcl.flags;

  /* update flow state */
  f->rkey = ans_mpcl.skey;

  uint8_t hash[SHA_DIGEST_LENGTH];
  hash_key_sha1(hash, f->rkey); // compute remote token
  f->rtok = *(token32*)hash;

  f->rcv_nxt = *(uint64_t *)(hash+SHA_DIGEST_LENGTH-sizeof(uint64_t)); // hash least significant 64 bits
  f->rcv_nxt = be64toh(f->rcv_nxt)+1; // MP_CAPABLE occupies 1 data level byte

  /* update subflow state */
  tcp_hdr_loc thl = tcp_hdr_dec(thn);
  in_sf->rcv_nxt = thl.seq+1;

  return TEST_CONT;
}

void b_init_sf_send_ack()
{
  subflow *sf = f->asf;

  mp_cap_loc mpcl = {
        .subtype = MPTCP_SUBTYPE_MP_CAPABLE,
        .version = MPTCP_VERSION,
        .flags = MP_CAP_A|MP_CAP_H, // TODO check if not sf->...
        .skey = f->lkey,
        .rkey = f->rkey
    };

  mp_cap_net mpcn = mp_cap_enc(&mpcl, TH_ACK);

  tcp_hdr_loc hdr = tcp_hdr_auto(sf, TH_ACK);
  send_tcp_seg(sf, &hdr, (u_char *)&mpcn, mpcn.length, NULL, 0);

  f->state = sf->state = ESTABLISHED;
}

int c_init_sf_recv_syn(C_ARGS)
{
  T_ASSERT_RET_ON_FAIL(packet!=NULL, "Send SYN on new connection", TEST_STOP);
  tcp_hdr_net *thn = get_tcp_hdr(packet);
  T_ASSERT_RET_ON_FAIL(thn->flags&&TH_SYN, "Send SYN on new connection", TEST_STOP);

  mp_cap_net *ans_mpcn = (mp_cap_net *) get_tcp_opt(TCP_OPT_MPTCP, MPTCP_SUBTYPE_MP_CAPABLE, thn);
  T_ASSERT_RET_ON_FAIL(ans_mpcn!=NULL, "SYN contains key", TEST_STOP);

  mp_cap_loc ans_mpcl = mp_cap_dec(ans_mpcn);
  T_ASSERT(is_set(MP_CAP_A, ans_mpcl.flags), "Flag A set to 1");
  T_ASSERT(!is_set(MP_CAP_B, ans_mpcl.flags), "Flag B set to 0");
  T_ASSERT((ans_mpcl.flags&&0x3F) == 0x01, "Flags C through H");
  T_ASSERT((ans_mpcl.version == 0), "MPTCP Version");
  in_sf->ci_o.mp_cap_flags_sut = ans_mpcl.flags;

  /* bind local subflow to remote port */
  tcp_hdr_loc thl = tcp_hdr_dec(thn);
  in_sf->rport = thl.sp;
  in_sf->raddr = ((ip_hdr_net *)(packet+SIZE_ETHER))->ip_src;

  f->rkey = ans_mpcl.skey;

  in_sf->rcv_nxt = thl.seq+1;
  in_sf->state = SYN_RECEIVED;

  return TEST_CONT;
}

void b_init_sf_send_synack()
{
  subflow *sf = f->asf;
  sf->isn = sf->snd_nxt;

  mp_cap_loc mpcl = {
      .subtype = MPTCP_SUBTYPE_MP_CAPABLE,
      .version = sf->ci_o.mp_cap_version,
      .flags = sf->ci_o.mp_cap_flags_ts,
      .skey = f->lkey
  };

  mp_cap_net mpcn = mp_cap_enc(&mpcl, TH_SYN|TH_ACK);
  tcp_hdr_loc hdr = tcp_hdr_auto(sf, TH_SYN|TH_ACK);

  if(sf->ci_o.synack_without_mp_cap == 0)
    send_tcp_seg(sf, &hdr, (u_char *)&mpcn, mpcn.length, NULL, 0);
  else
    send_tcp_seg(sf, &hdr, 0, 0, NULL, 0);

  f->snd_nxt++;
  sf->snd_nxt++;
}

/**
 * Match 2nd packet of the handshake (SYN+ACK) and evaluate its conformance.
 */
int c_init_sf_recv_ack(C_ARGS)
{
  T_ASSERT_RET_ON_FAIL(packet!=NULL, "Send ACK upon SYN/ACK reception", TEST_STOP);
  tcp_hdr_net *thn = get_tcp_hdr(packet);

  mp_cap_net *mpcn = (mp_cap_net *) get_tcp_opt(TCP_OPT_MPTCP, MPTCP_SUBTYPE_MP_CAPABLE, thn);

  if(f->asf->ci_o.synack_without_mp_cap==0) {
    T_ASSERT_RET_ON_FAIL(mpcn!=NULL, "ACK contains sender and receiver keys (no mp_capable)", TEST_STOP);
  }
  else {
    T_ASSERT(mpcn==NULL, "SYN/ACK without MP_CAPABLE");
    return TEST_STOP;
  }

  T_ASSERT_RET_ON_FAIL(mpcn->length==20, "ACK contains sender and receiver keys (wrong option length)", TEST_STOP);
  mp_cap_loc mpcl = mp_cap_dec(mpcn);
  T_ASSERT(mpcl.version==0, "MPTCP Version");
  T_ASSERT_RET_ON_FAIL(
      memcmp(&mpcl.skey, &f->rkey, sizeof(key64))==0 &&
      memcmp(&mpcl.rkey, &f->lkey, sizeof(key64))==0,
      "ACK contains sender and receiver keys (changed key)", TEST_STOP);

  // no checksum allowed only if both hosts agree
  T_ASSERT((mpcl.flags & MP_CAP_A) || (!(in_sf->ci_o.mp_cap_flags_ts & MP_CAP_A) &&
      !(in_sf->ci_o.mp_cap_flags_sut & MP_CAP_A)), "Checksums");

  f->state = in_sf->state = ESTABLISHED;
  return TEST_CONT;
}

/**
 * Perform a new connection handshake TS->SUT on subflow \c sf.
 * @return TEST_CONT or TEST_STOP
 */
int init_sf_handshake(subflow *sf)
{
  f->asf = (sf==NULL)? f->asf: sf;
  int ret;

  T_CHECK((ret=send_recv(b_init_sf_send_syn, &(handler_t){.f=c_init_sf_recv_synack})));

  b_init_sf_send_ack();

  cleanup:
  return ret;
}

/**
 * Perform a new connection handshake SUT->TS on subflow \c sf.
 * @return TEST_CONT or TEST_STOP
 */
int init_sf_handshake_sutts(subflow *sf)
{
  int ret;
  // receive initial SYN
  T_CHECK((ret=send_recv(b_init_sf_remote_send_syn,
      &(handler_t){.f=c_init_sf_recv_syn})));
  // sent SYN+ACK and receive ACK
  T_CHECK((ret=send_recv(b_init_sf_send_synack,
      &(handler_t){.f=c_init_sf_recv_ack})));

  cleanup:
  return ret;
}

void ts_init_ts_sut()
{
  test_case_init(1);

  u_char mp_cap_flags_cmb;
  u_char mp_cap_flags_cmb_array[] = {
      MP_CAP_A|MP_CAP_H,
      MP_CAP_A,
      MP_CAP_A|MP_CAP_B|MP_CAP_H,
      MP_CAP_B|MP_CAP_H,
      0
  };

  int syn_without_mp_cap;
  int syn_without_mp_cap_array[] = {0, 1};

  u_char mp_cap_version;
  u_char mp_cap_version_array[] = {MPTCP_VERSION, MPTCP_VERSION + 1};

  ALT_GROUP(g)
  {
    ALT_GROUP_PREAMBLE(g);

    syn_without_mp_cap = ALT(&g, 1, syn_without_mp_cap_array);

    if (!syn_without_mp_cap)
      mp_cap_version = ALT(&g, 2, mp_cap_version_array);
    else
	  mp_cap_version = MPTCP_VERSION;

    if (!syn_without_mp_cap && mp_cap_version == MPTCP_VERSION)
      mp_cap_flags_cmb = ALT(&g, 3, mp_cap_flags_cmb_array);
    else
      mp_cap_flags_cmb = 0;

    f->asf->ci_o.syn_without_mp_cap = syn_without_mp_cap;
    f->asf->ci_o.mp_cap_version = mp_cap_version;
    f->asf->ci_o.mp_cap_flags_ts = mp_cap_flags_cmb;
    send_recv(b_init_sf_send_syn, &(handler_t){.f=c_init_sf_recv_synack});
    b_sf_send_rst(f->sf[0]);
  }

  test_case_free();
}

void ts_init_sut_ts()
{
  test_case_init(1);

  int synack_without_mp_cap[] = {0, 1};
  u_char flags[] = {
      MP_CAP_A|MP_CAP_H,
      MP_CAP_H
  };

  int i, j;

  ARRAY_FOREACH_IDX(i, synack_without_mp_cap)
  ARRAY_FOREACH_IDX(j, flags)
  {
    f->sf[0]->ci_o.mp_cap_version = MPTCP_VERSION;
    f->sf[0]->ci_o.mp_cap_flags_ts = flags[j];
    f->sf[0]->ci_o.synack_without_mp_cap = synack_without_mp_cap[i];
    send_recv(b_init_sf_remote_send_syn, &(handler_t){.f=c_init_sf_recv_syn});
    send_recv(b_init_sf_send_synack, &(handler_t){.f=c_init_sf_recv_ack});
    b_sf_send_rst(f->sf[0]);
  }

  test_case_free();
}

/**
 * Send a SYN+MP_CAP and receive a SYN+ACK. Then, send an ACK with wrong keys.
 * The SUT is expected to ignore it. Finally, try to establish a new subflow
 * using
 *
 *   - the remote token computed from the initial SYN/ACK: we expect the
 *     handshake to succeed. In particular, the HMAC included in the
 *     SYN/ACK+MP_JOIN should have been computed from the original keys.
 *
 *   - the token computed from the key included in the wrong ACK: the SUT
 *     should respond with an RST.
 */
void ts_init_wrong_ack()
{
  test_case_init(2);
  dss_loc out_dss;
  key64 lkey_orig, rkey_orig, lkey_ack, rkey_ack;

  // use wrong keys for new subflow token
  int new_sf_token_keys_ack_array[] = {0, 1};

  ALT_GROUP(g)
  {
    ALT_GROUP_PREAMBLE(g);
    f->asf = f->sf[0];

    /* Send SYN and receive SYN+ACK */
    handler_t synack = {.f = c_init_sf_recv_synack};
    register_handler(&synack);
    b_init_sf_send_syn();
    recv_packet(&synack);

    lkey_orig = f->lkey;
    rkey_orig = f->rkey;

    /* Send wrong ACK */
    f->lkey = lkey_ack = *(key64 *)(uint8_t[8])
        {0xFA, 0x11, 0x5A, 0xFE, 0xED, 0x1F, 0x1C, 0xE5};
    f->rkey = rkey_ack = *(key64 *)(uint8_t[8])
        {0xB1, 0x11, 0xAB, 0x1E, 0xBA, 0x51, 0x11, 0xCA};

    b_init_sf_send_ack();

    /* Send one data byte and receive DATA_ACK */
    handler_t dack = {
        .f = c_recv_dack,
        .args = &(struct c_recv_dack_arg) {.dack = f->snd_nxt + 1}
    };
    register_handler(&dack);

    u_char data[] = {'*'};
    out_dss = dss_autofill(8, 8, data, sizeof(data), 1, 0);
    b_send_data(f->sf[0], &out_dss, data, sizeof(data), TH_ACK);

    if(!T_ASSERT(recv_packet(&dack)==TEST_CONT, "Wrong ACK should be ignored"))
      goto cleanup;

    /* Open a new subflow */
    f->lkey = lkey_orig;
    f->rkey = rkey_orig;

    if(ALT(&g, 2, new_sf_token_keys_ack_array))
    {
      f->rtok = compute_token(rkey_ack);
      f->sf[1]->ns_o.syn_unknown_token = 1;
    }
    else
      f->sf[1]->ns_o.syn_unknown_token = 0;

    T_CHECK(new_sf_handshake(f->sf[1]));

    /* Postamble */
    cleanup:
    b_flow_fastclose();
    b_remote_send_data(0, 1);
    f->lkey = lkey_orig;
  }

  test_case_free();
}

/**
 * Send initial SYNs with malformed MPTCP option. Different option lengths and
 * subtype considered. For subtype MP_CAPABLE, we try different values of MPTCP
 * version.
 */
void ts_init_syn_fuzz()
{
  test_case_init(1);
  f->sf[0]->rport = remote_get_port();

  tcp_hdr_loc hdr = tcp_hdr_auto(f->sf[0], TH_SYN);

  u_char opt[64];

  int olen[] = {1, 12, 16, 20, 24};
  int version[] = {0, 7, 15};
  int subtype[] = {
    MPTCP_SUBTYPE_MP_CAPABLE,
    MPTCP_SUBTYPE_DSS,
    MPTCP_SUBTYPE_MP_FAIL
  };

  ALT_GROUP(g)
  {
    ALT_GROUP_PREAMBLE(g);

    int l = ALT(&g, 1, olen);
    int s = ALT(&g, 2, subtype);

    *(mptcp_hdr_pre *)&opt = (mptcp_hdr_pre){
        .type = TCP_OPT_MPTCP,
        .length = l,
        .subtype = s << 4,
    };

    if(s == MPTCP_SUBTYPE_MP_CAPABLE)
    {
      int v = ALT(&g, 3, version);
      ((mp_cap_net *)&opt)->subver += v;
    }
    send_tcp_seg(f->sf[0], &hdr, (u_char *)&opt, l, NULL, 0);
    usleep(10e3);
    b_sf_send_rst(f->sf[0]);
  }

  test_case_free();
}
