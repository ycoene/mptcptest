/**
 * @file      barrier.h
 * @brief     Barrier.
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */
#ifndef BARRIER_H_
#define BARRIER_H_

/* Reference: http://siber.cankaya.edu.tr */

/**
 * Barrier. Used for signaling/waiting on handler match arrival.
 *
 * We call this barrier but in fact only one thread waits for \c count_lock at
 * a time, until another one comes unblocking it by signaling \c ok_to_proceed.
 */
typedef struct {
  pthread_mutex_t count_lock;
  pthread_cond_t ok_to_proceed;
  int match_count;
  struct hlist_node *hln;
} barrier_t;

/**
 * Initialize a barrier.
 */
void mylib_init_barrier(barrier_t *b);

#endif /* BARRIER_H_ */
