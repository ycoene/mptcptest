/**
 * @file      packet_handler.h
 * @brief     Packet handler type and operations.
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#ifndef PACKET_HANDLER_H_
#define PACKET_HANDLER_H_

//#define DEBUG

#include <pthread.h>
#include <pcap.h>
#include "mptcp.h"
#include "testcases.h"

#define C_ARGS void *args, const struct pcap_pkthdr *hdr, const u_char *packet, subflow *in_sf

/**
 * This mutex is locked during the time of processing an incoming packet. As a
 * result, incoming packets are processed sequentially.
 */
pthread_mutex_t pcap_callback_mutex;

#define SUSPEND_PACKET_TREATMENT() \
  if(pthread_mutex_lock(&pcap_callback_mutex)!=0){perror("ERROR on pthread_mutex_lock");}

#define RESUME_PACKET_TREATMENT() \
  if(pthread_mutex_unlock(&pcap_callback_mutex)!=0){perror("ERROR on pthread_mutex_unlock");}

typedef struct handler_match
{
  TAILQ_ENTRY(handler_match) entries;

  struct pcap_pkthdr hdr; /**< pcap received packet header */
  u_char *packet; /**< pointer to the received packet */
  subflow *in_sf; /**< subflow the handled packet was received from */
  int retval; /** return value of handler function */
} handler_match;

typedef struct handler
{
  LIST_ENTRY(handler) entries;

  int (*f)(C_ARGS); /**< handler function */
  void *args; /**< arguments for handler function */
  subflow *sf; /**< target subflow */

  struct handler_set *hs; /**< handler set this handler belongs to, if any */
  struct hln *hsl_member;
  int copy_packet; /**< boolean value for copying received packet or not */
  int reusable; /**< must be manually unregistered. matching packets enqueued */

  TAILQ_HEAD(match_list, handler_match) match_queue_head;

  struct pcap_pkthdr hdr; /**< pcap received packet header */
  u_char *packet; /**< pointer to the received packet */
  subflow *in_sf; /**< subflow the handled packet was received from */
  int retval; /** return value of handler function */
  barrier_t barrier; /** used when waiting for matching packet */
} handler_t;

/**
 * Evaluate registered handlers against incoming packet. For every matching
 * handler \c h, execute \c h's callback function, remove \c h from \c hl and
 * notify waiting threads.
 */
void hlist_exec_matching(const struct pcap_pkthdr *header, const u_char *packet, subflow *in_sf, struct hlist *hl);

/**
 * Execute \c h's callback function with NULL arguments and remove \c h it from
 * \c hl. Executed upon \c recv_packet(\c h) timeout.
 */
void hlist_exec_timeout(handler_t *h, struct hlist *hl);

/**
 * libpcap callback function. In this case, \c args is used to pass a \c
 * subflow pointer.
 */
void pcap_callback(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);

int register_handler(handler_t *h);
int handler_reset(handler_t *h);
int unregister_handler(handler_t *h);
int handler_unregister_all();

/**
 * Execute function \c send_f, register handler \c h and wait for a match.
 */
int send_recv(void (*send_f)(), handler_t *h);

int handler_is_ready(handler_t *h);

/**
 * Wait until reception of a matching packet and fill \c h's output values.
 * Timeout after 1 second.
 */
int recv_packet(handler_t *h);

struct hln {
  LIST_ENTRY(hln) entries;
  handler_t *h;
};

/**
 * Handler set.
 */
struct handler_set {
  LIST_HEAD(hl, hln) hl;
  pthread_mutex_t mutex;
  pthread_cond_t cond;
  int count;
};

void handler_set(handler_t *h, struct handler_set *hs);

void handler_clr(handler_t *h, struct handler_set *hs);

void handler_zero(struct handler_set *hs);

void handler_set_init(struct handler_set *hs);

void handler_set_destroy(struct handler_set *hs);

handler_t *handler_select(struct handler_set *hs, int nonblock);

#endif /* PACKET_HANDLER_H_ */
