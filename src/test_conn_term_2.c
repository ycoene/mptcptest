/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "packet_handler.h"
#include "testcases.h"

int dack_len = 4;
int dsn_len = 4;
int dack_sent = 0;

handler_t dfin;

int rem_dfin_last_ongoing = 1;
int rem_ongoing_data_on_other_sf = 0;
int rem_not_all_data_acked = 0;
int rem_open_sf_count;

void rem_snd_dfin()
{
  b_remote_send_data(0, 1);
}

void reg_dfin(handler_t *dfin)
{
  *dfin = (handler_t){.f=c_recv_data_fin, .copy_packet=1};
  register_handler(dfin);
}

/**
 * Close connection by remote end
 */
int vrf_dfin_recvd(handler_t *dfin)
{
  // wait for DFIN
  T_CHECK(recv_packet(dfin));

  if(get_tcp_hdr(dfin->packet)->flags & TH_FIN)
  {
    T_ASSERT(!rem_ongoing_data_on_other_sf, "Combined DATA_FIN and subflow FIN");
  }
  return TEST_CONT;
  cleanup: return TEST_STOP;
}

void snd_dack_of_dfin()
{
  dss_loc dssl = dss_autofill(dack_len, 0, NULL, 0, 0, 0);
  b_send_data(NULL, &dssl, NULL, 0, TH_ACK);
  dack_sent = 1;
}

/**
 * Receive subflow FIN and acknowledge it
 */
int c_recv_fin_reply_ack(C_ARGS)
{
  if(packet == NULL)
    return TEST_IRRELEVANT_PKT;
  if(!(get_tcp_hdr(packet)->flags & TH_FIN))
    return TEST_IRRELEVANT_PKT;

  // Check for premature subflow termination
  if(--rem_open_sf_count == 0) // all subflows closed by remote end
  {
    if(!dack_sent)
    {
      // DFIN must be the last ongoing segment
      dss_loc dssl = dss_dec((dss_net *)get_tcp_opt(TCP_OPT_MPTCP, MPTCP_SUBTYPE_DSS, get_tcp_hdr(packet)));
      T_ASSERT(dssl.dsn == f->rcv_nxt, "Premature subflow termination");
    }
  }

  // Acknowledge subflow FIN
  dss_loc dssl = dss_autofill(dack_len, 0, NULL, 0, 0, 0);
  b_send_data(in_sf, &dssl, NULL, 0, TH_ACK);

  return TEST_CONT;
}

/**
 * Acknowledge subflow FINs upon reception (non-blocking)
 */
void reg_fins_snd_acks(handler_t in_fin[])
{
  int i;
  for(i=0; i<f->nsf; i++)
  {
    in_fin[i] = (handler_t) {.f=c_recv_fin_reply_ack, .sf=f->sf[i]};
    register_handler(&in_fin[i]);
  }
}

void snd_dfin()
{
  dss_loc dssl = dss_autofill(dack_len, dsn_len, NULL, 1, 0, 1);
  b_send_data(NULL, &dssl, NULL, 0, TH_ACK);
}

void snd_fins_reg_acks(handler_t in_ack[])
{
  int i;
  for(i=0; i<f->nsf; i++)
  {
    dss_loc dssl = dss_autofill(dack_len, 0, NULL, 0, 0, 0);
    b_send_data(f->sf[i], &dssl, NULL, 0, TH_FIN|TH_ACK);
    in_ack[i] = (handler_t) {.f=c_recv_ack};
    register_handler(&in_ack[i]);
  }
}

void reg_dack_of_dfin(handler_t *dack)
{
  *dack = (handler_t) {.f=c_recv_dack};
  register_handler(dack);
}

int rcv_dack_of_dfin(handler_t *dack)
{
  T_CHECK(recv_packet(dack));

  return TEST_CONT;
  cleanup:
  CU_FAIL("DATA_FIN not DATA_ACKed");
  return TEST_STOP;
}

/**
 * Verify if subflow FINs received
 */
int vrf_fins_recvd(handler_t in_fin[])
{
  int i;
  for(i=0; i<f->nsf; i++)
  {
    if(recv_packet(&in_fin[i])!=TEST_CONT)
    {
      CU_FAIL("Subflows termination");
      return TEST_STOP;
    }
  }
  return TEST_CONT;
}

int vrf_acks_recvd(handler_t in_ack[])
{
  int i;
  for(i=0; i<f->nsf; i++)
  {
    if(recv_packet(&in_ack[i])!=TEST_CONT)
    {
      CU_FAIL("Last subflow ACK not received");
      return TEST_STOP;
    }
  }
  return TEST_CONT;
}

int dfin_exchange_1()
{
  handler_t in_fin[MAX_NB_SF], in_ack[MAX_NB_SF], dack;

  SUSPEND_PACKET_TREATMENT();
  rem_snd_dfin();
  reg_dfin(&dfin);
  reg_fins_snd_acks(in_fin);
  RESUME_PACKET_TREATMENT();

  T_CHECK(vrf_dfin_recvd(&dfin));

  usleep(20e3); // to make sure all subflows are not closed if ongoing data, before sending a DACK

  if(rem_not_all_data_acked == 1)
  {
    if(payload_len(&dfin.hdr, dfin.packet)==0) // DFIN and data not combined
      // finally received lost segment
      f->rcv_nxt++;
  }
  if(rem_ongoing_data_on_other_sf)
  {
    f->rcv_nxt++;
  }

  SUSPEND_PACKET_TREATMENT();
  snd_dack_of_dfin();
  rem_not_all_data_acked = 1;
  RESUME_PACKET_TREATMENT();

  SUSPEND_PACKET_TREATMENT();
  snd_dfin();
  snd_fins_reg_acks(in_ack);
  reg_dack_of_dfin(&dack);
  RESUME_PACKET_TREATMENT();

  T_CHECK(rcv_dack_of_dfin(&dack));
  T_CHECK(vrf_fins_recvd(in_fin));
  T_CHECK(vrf_acks_recvd(in_ack));

  return TEST_CONT;

  cleanup:
  CU_FAIL("Flow termination failed.");
  return TEST_STOP;
}

void ts_close_sut_ts_2()
{
  test_case_init(2);
  rem_open_sf_count = 2;

  rem_not_all_data_acked = 1;
  rem_ongoing_data_on_other_sf = 1;

  T_CHECK(init_sf_handshake(f->sf[0]));

  if(rem_ongoing_data_on_other_sf)
  {
    b_remote_send_data(1,0);
  }

  f->asf = f->sf[1];
  T_CHECK(new_sf_handshake(f->sf[1]));

  if(rem_not_all_data_acked == 1)
  {
    // send data we will not ack until sending the DACK[DFIN]
    b_remote_send_data(1,0);
  }

  T_CHECK(dfin_exchange_1());

  goto exit;

  cleanup:
  b_flow_fastclose();

  exit:
  test_case_free();
}
