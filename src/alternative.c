/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "alternative.h"
#include <assert.h>

int alt(struct alt_grp *grp, int alt_id, int card)
{
  assert(alt_id != 0);
  if(grp->take_any_path)
  {
    grp->n[alt_id].i = 0;
    grp->n[alt_id].exhausted = 0;
    return grp->n[alt_id].i;
  }

  if(grp->n[alt_id].exhausted)
  {
    if(grp->n[alt_id].i+1 < card)
    {
      grp->n[alt_id].i += 1;
      grp->n[grp->last].exhausted = 0;
      grp->n[alt_id].exhausted = 1;
    }
    else
    {
      grp->n[alt_id].i = 0;
      grp->n[alt_id].exhausted = 0;
      grp->take_any_path = 1;
    }
  }
  else
  {
    grp->n[grp->last].exhausted = 0;
    grp->n[alt_id].exhausted = 1;
  }

  grp->last = alt_id;
  return grp->n[alt_id].i;
}
