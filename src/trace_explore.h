/**
 * @file      trace_explore.h
 * @brief     Trace explorer.
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "testcases.h"
#include <sys/queue.h>

#define TIMEOUT 10

struct state;
char *err_str;

/**
 * Token.
 */
typedef struct token {
  LIST_ENTRY(token) entries;
  char *to_str; ///< string representation
  enum type {PASSIVE, ACTIVE} type;
  union {
    // active token
    struct {
      void (*f)(); ///< stimulus function
    };
    // passive token
    struct {
      handler_t h;
      int (*must_arrive)(struct token *this); ///< whether the token \e must be issued eventually
    };
  };
  int (*enabled)(struct token *this); ///< whether the token may be issued in current state
  void (*on_set)(struct token *this); ///< state transition function
} token_t;

typedef struct trace_node {
  TAILQ_ENTRY(trace_node) entries;
  token_t *tok;
  int exhausted;
} trace_node;

TAILQ_HEAD(trace, trace_node);

/**
 * @brief Explore a token trace.
 *
 * Explore a token trace based on a previous trace.
 *
 * @param s representation of the state of the connection
 * @param tok_array array of considered tokens
 * @param trace previous trace or empty pointer if first call
 * @param random random trace generated if non-null
 * @retval TEST_CONT trace completed successfully
 * @retval TEST_STOP trace completed with error
 * @retval TEST_DONE all traces explored
 */
int explore_trace(
    token_t *tok_array[],
    uint tok_array_len,
    int (*end_trace_cond)(),
    struct trace *trace,
    int random
);
