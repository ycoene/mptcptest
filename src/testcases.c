/**
 * @author    Yvan Coene
 * @copyright BSD 2-Clause License
 */

#include "packet_handler.h"
#include "server.h"
#include "tcp.h"
#include "testcases.h"
#include <assert.h>
#include <CUnit/CUnit.h>
#include <openssl/hmac.h>
#include <pthread.h>
#include <string.h>
#include <time.h>

inline int is_set(u_int flag, u_int flags) {
  return (flag & flags)!=0;
}

void hash_key_sha1(uint8_t *hash, key64 key) {
  SHA_CTX ctx;
  SHA1_Init(&ctx);
  SHA1_Update(&ctx, &key, sizeof(key));
  SHA1_Final(hash, &ctx);
}

/**
 * Compute HMAC-A = HMAC(Key=(Key-A+Key-B), Msg=(R-A+R-B))
 * @param dst destination buffer of size \c SHA_DIGEST_LENGTH
 * @param k1 Key-A
 * @param k2 Key-B
 * @param r1 R-A
 * @param r2 R-B
 */
void hmac_sha1(uint8_t *dst, const key64 *k1, const key64 *k2, const token32 *r1, const token32 *r2)
{
  HMAC_CTX ctx;
  uint8_t key[2*sizeof(key64)];
  uint8_t msg[2*sizeof(token32)];
  memcpy(&key, k1, sizeof(key64));
  memcpy(((key64*)&key)+1, k2, sizeof(key64));
  memcpy(&msg, r1, sizeof(token32));
  memcpy(((token32*)&msg)+1, r2, sizeof(token32));
  uint len;
  HMAC_Init(&ctx, key, sizeof(key), EVP_sha1());
  HMAC_Update(&ctx, msg, sizeof(msg));
  HMAC_Final(&ctx, dst, &len);
}

token32 compute_token(const key64 key)
{
  uint8_t hash[SHA_DIGEST_LENGTH];
  hash_key_sha1(hash, key);
  return *(token32*)hash;
}

int b_sf_send_rst(subflow *sf)
{
  tcp_hdr_loc hdr = tcp_hdr_auto(sf, TH_RST);
  send_tcp_seg(sf, &hdr, NULL, 0, NULL, 0);
  return 0;
}

void b_sf_abort_all() {
  int i;
  for(i=0; i<f->nsf; i++) {
    b_sf_send_rst(f->sf[i]);
  }
}

int c_recv_rst(C_ARGS)
{
  if(packet==NULL)
    return TEST_STOP;

  if(!(get_tcp_hdr(packet)->flags & TH_RST))
    return TEST_IRRELEVANT_PKT;

  return TEST_CONT;
}

void test_case_init(int nsf)
{
  assert(nsf != 0);
  f = flow_init();
  f->nsf = nsf;
  int i;
  for(i=0; i<nsf; i++)
  {
    assert(cfg.loc_addr[i] != NULL);
    f->sf[i] = subflow_init(cfg.loc_addr[i]);
    f->sf[i]->id = i;
  }
  f->asf = f->sf[0];
}

void test_case_free()
{
  int i;
  for(i=0; i<f->nsf; i++)
  {
    assert(f->sf[i] != NULL);
    subflow_free(f->sf[i]);
  }
  handler_unregister_all();
  free(f);
}
